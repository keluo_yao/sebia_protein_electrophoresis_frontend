import React, { Component, PropTypes } from 'react';
import { Button, Icon } from 'antd';
import { connect } from 'react-redux';
import { dataDrawerL2_DataMassage, sebiaSubtyping_Clear } from './actions/data.js';




class Button_Backward_Forward extends Component {

    constructor(props) {
        super(props);
        this.state = {
          backwardType: "Default",
          forwardType: "Default"
        };
      }

    handleBackClick = (drawerContentLocalL2, queryKey, listKey) => {

        if (listKey > 1) {
           this.props.handleDrawerAction(drawerContentLocalL2[queryKey].spepPgValue[listKey - 1], listKey - 1); 
           this.setState({
            backwardType: "Default",
          });
        } else {
            this.setState({
                backwardType: "danger",
              });
        }   
    }

    handleForwardClick = (drawerContentLocalL2, queryKey, listKey) => {

        if (listKey >= Object.keys(drawerContentLocalL2[queryKey].spepPgValue).length) {
            this.setState({
                forwardType: "danger",
              });
        } else {
            this.props.handleDrawerAction(drawerContentLocalL2[queryKey].spepPgValue[listKey + 1], listKey + 1);
            this.setState({
                forwardType: "Default",
              });

        }   
    }


	render () {

        return (
            <div>
                <Button.Group size='large'>
                <Button type="Default"
                    onClick={()=>this.handleBackClick(this.props.queryResultLocal, this.props.queryKeyLocal, this.props.listKeyLocal)}>
                    <Icon type="left" />Back
                </Button>
                <Button type="Default"
                    onClick={()=>this.handleForwardClick(this.props.queryResultLocal, this.props.queryKeyLocal, this.props.listKeyLocal)}>
                    Next<Icon type="right" />
                </Button>
                </Button.Group>
            </div>
            )
    }
}

const mapStateToProps = (state) => {
    return {
        queryResultLocal: state.dataSpepPgSuccess,
        queryKeyLocal: state.dataDrawerL1.queryKey,
        listKeyLocal: state.updateListKey,

    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleDrawerAction: (drawerContentLocalL2, curvedata, key) => {
					dispatch(sebiaSubtyping_Clear());
					dispatch(dataDrawerL2_DataMassage(drawerContentLocalL2, curvedata, key));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Button_Backward_Forward);