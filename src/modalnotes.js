import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';
import { connect } from 'react-redux';
import { Modal } from 'antd';
import { modalNotes } from './actions/data.js';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const { TextArea } = Input;

class Modalnotes extends Component {

    handleClick = () => {
        this.props.handleModalAction()
      }

    convertToPlain =(rtf) => {
      rtf = rtf.replace(/\\par[d]?/g, "");
      rtf = rtf.replace(/\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?/g, "");
      return rtf.replace(/\\'[0-9a-zA-Z]{2}/g, "").trim();
    }

	render ()
		{
      if (Object.keys(this.props.dataNotesLocal).length > 0)
        {
            return (
                <div>
                    <Modal
                    title='Note'
                    visible={this.props.modalNotesVisiblelocal}
                    width='70%'
                    onOk={()=>this.handleClick()}
                    onCancel={()=>this.handleClick()}
                    >

                        <p>{this.props.dataNotesLocal.data.results.Note.Note.Author}</p>

                        {/* <TextArea value={this.convertToPlain(this.props.dataNotesLocal.data.results.Note.Note.Text)} placeholder="Autosize height based on content lines" autosize /> */}
                        <ReactQuill value={this.props.dataNotesLocal.data.results.Note.Note.Text} />
                        <p>{this.props.dataNotesLocal.data.results.Note.Note.Date}</p>


                    </Modal>
            </div>
            )
        } else {
          return (
            <div></div>
          )
        }
	}
}

const mapStateToProps = (state) => {
    return {
        modalNotesVisiblelocal: state.modalNotes,
        dataNotesLocal: state.dataNotesSuccess,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleModalAction: () => {
          dispatch(modalNotes(false));
        },
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(Modalnotes);