import React, { Component } from 'react';
import PatientInfo from './patientinfo.js';
import RightNavBar from './rightnavbar.js';
import Modallabs from './modallabs.js';
import Modalnotes from './modalnotes.js';
import Modalrads from './modalrads.js';
import Speplab from './speplab.js';

class RightDisplayContent extends Component {

    render ()
    {


                    return (
                    		<div>
                    		    <PatientInfo />

                    			<RightNavBar />
                                <Modallabs />
                                <Modalnotes />
                                <Modalrads />
                            </div>
                      )
    }

}

export default RightDisplayContent;