import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';
import { Input } from 'antd';
import { Button } from 'antd';
import { connect } from 'react-redux';
import { processModalNotes } from './actions/data.js';


const { Column, ColumnGroup } = Table;

class Notes extends Component {

  handleClick = (recordNoteID, localMRN1) => {
      this.props.handleModalAction(recordNoteID, localMRN1)
  }


	render ()
		{
      if (Object.keys(this.props.localNotesIDs).length > 0)
        {
            return (
                <div>
                    <Table dataSource={this.props.localNotesIDs.data.results.Notes2} size="small" pagination={false}>
                    <ColumnGroup>
                        <Column
                        title='Note ID'
                        key='NoteId'
                        render={(record) => (
                          <span>
                            <Button type="primary" onClick={()=>this.handleClick(record.NoteId, this.props.localNotesIDs.data.results.Information2[0].MRN)}>
                              <a href="javascript:;">{record.NoteId}</a>
                            </Button>
                          </span>
                        )}
                        />						
                        <Column
                          title='Date of Service'
                          dataIndex='DateOfService'
                          key='DateOfService'
                        />
                        <Column
                          title='NoteType'
                          dataIndex='NoteType.Title'
                          key='NoteType.Title'
                        />
                      </ColumnGroup>
                    </Table>
            </div>
            )
          } else {
            return (
              <div>No data</div>
            )
          }
	}
}

const mapStateToProps = (state) => {
    return {
      localNotesIDs: state.dataAxiosSuccess,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

  return {
      handleModalAction: (recordNoteID, localMRN1) => {
        dispatch(processModalNotes(recordNoteID, localMRN1));
      },
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Notes);