import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Switch } from 'antd';
import { Row, Col } from 'antd';
import { Card } from 'antd';
import { demo_mode_change } from './actions/data.js';





class Button_Demo_Mode extends Component {


    handleClick = (check) => {
        this.props.handleClickAction(check)
    }


	render () {

        return (
            <div>
                <Card bodyStyle={{ padding: 2, color: '#FFFFFF', background: '#00274C'}} bordered={false}>               
                <Row type="flex" justify="center" align="middle">
                    <Col>
                        Demo Mode
                    </Col>
                    <Col>
                        <div style={{ display: 'inline-flex', justifyContent: 'center', alignItems: 'center', padding: 5}}> 
                            <Switch checkedChildren="on" unCheckedChildren="off" onChange={(onChange) => this.handleClick(onChange)} />
                        </div>
                    </Col>
                </Row>
                </Card> 
                
            </div>
            )
    }
}

const mapStateToProps = (state) => {
    return {
        checklocal: state.demo_mode,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleClickAction: (checked) => {
		    dispatch(demo_mode_change(checked));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Button_Demo_Mode);