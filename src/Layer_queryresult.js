import React, { Component, PropTypes } from 'react';
import { Tabs } from 'antd';
import { connect } from 'react-redux';
import QueryResult_Show from './queryresult_show.js';


const TabPane = Tabs.TabPane;

class Layer_QueryResult extends Component {



    render ()
		{

        if (Object.keys(this.props.queryResultLocal).length > 0) {
            if (Object.keys(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue).length > 0) {
                return (
                    <div>
                        <QueryResult_Show />
                    </div>    
                )
            } else {
                return (
                    <div>
                        <p><font color="black">Nothing To Display</font></p>
                    </div>    
                )
            }
    
        } else {
            return (
                <div>
                    <p><font color="black">Nothing To Display</font></p>
                </div>

            )

        }



	}
}

const mapStateToProps = (state) => {
    return {
        queryResultLocal: state.dataSpepPgSuccess,
        queryKeyLocal: state.dataDrawerL1.queryKey
    };
};

export default connect(mapStateToProps, null)(Layer_QueryResult);