import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Tabs } from 'antd';
import { selectRightNavBar } from './actions/data.js';
import Essentials from './essentials.js';
import Medications from './medications.js';
import Radiology from './radiology.js';
import Notes from './Notes.js';
import Speplab from './speplab.js';

const TabPane = Tabs.TabPane;

class RightNavBar extends Component {

    handleClick = e => {
//        this.props.selectLeftNavBar()
        this.props.handleClickAction(e);
    }



    render ()
		{

        return (
                <div>
                  <Tabs onChange={this.handleClick} type="card">
                    <TabPane tab="Protein Electrophoresis" key="1"><Speplab /></TabPane>
                    <TabPane tab="Laboratory" key="2"><Essentials /></TabPane>
                    <TabPane tab="Medications" key="3"><Medications /></TabPane>
                    <TabPane tab="Radiology" key="4"><Radiology /></TabPane>
                    <TabPane tab="Notes" key="5"><Notes /></TabPane>                                           
                  </Tabs>
                </div>


            )

	}
}

const mapStateToProps = (state) => {
    return {
        rightNavBarSelection: state.selectRightNavBar,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleClickAction: (keySelected) => {
            dispatch(selectRightNavBar(keySelected));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RightNavBar);