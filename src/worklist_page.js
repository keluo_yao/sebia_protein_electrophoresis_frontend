import React, { Component, PropTypes } from 'react';
import Worklist_Portal from './worklist_portal';
import Worklist from './worklist.js';

import { Layout } from 'antd';

const { Header, Footer, Sider, Content } = Layout;

  
class Worklist_Page extends Component {    
    

	render () {

        return (
                <div>
                    <Header style={{ background: '#225E8F' }}>
                    <Worklist_Portal />
                    <P>Worklist</P>
                    </Header>


                <Worklist />

            </div>
 
            );
        }
}

export default Worklist_Page;