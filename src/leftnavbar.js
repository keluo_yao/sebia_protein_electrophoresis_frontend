import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Card } from 'antd';
import { Menu, Icon } from 'antd';
import { selectLeftNavBar } from './actions/data.js';
import EpicStatus from './epicstatus.js';
import SebiaStatus from './sebiastatus.js';
import Button_Demo_Mode from './button_demo_mode';

class LeftNavBar extends Component {

    handleClick = e => {
//        this.props.selectLeftNavBar()
        this.props.handleClickAction(e.key);
    }



    render ()
		{

        return (
                <div>
                                            <Card bodyStyle={{ padding: 10, background: '#00274C' }} bordered={false}>
                                            <div>
                                              <img alt="example" width="100%" src="./img/u_mich_M.png" />
                                            </div>

                                              <h5 style={{ padding: '5px'}}>Sebia Capillarys Dashboard With Epic Integration - Alpha Testing Version </h5>
                                              <h5 style={{ padding: '5px'}}>Epic connection status </h5>
                                              <EpicStatus />
                                              <h5 style={{ padding: '5px'}}>Sebia connection status </h5>
                                              <SebiaStatus />
                                          </Card>



                                          <Menu
                                            onClick={this.handleClick}
                                            selectedKeys={this.props.leftNavBarSelection}
                                            mode="horizontal"
                                            style={{ background: '#00274C', color: '#FFFFFF'}}
                                          >
                                            <Menu.Item key="landingpage">
                                            <Icon type="home" />Home
                                            </Menu.Item>
                                            <Menu.Item key="query">
                                              <Icon type="search" />Query
                                            </Menu.Item>
                                            <Menu.Item key="MiChart">
                                              <Icon type="search" />MiChart
                                            </Menu.Item>
                                                                                         
                                          </Menu>
                                          <Card bodyStyle={{ padding: 10, background: '#00274C'}} bordered={false}>
                                            <Button_Demo_Mode />
                                          </Card>

                </div>


            )

	}
}

const mapStateToProps = (state) => {
    return {
        leftNavBarSelection: state.selectLeftNavBar,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleClickAction: (keySelected) => {
            dispatch(selectLeftNavBar(keySelected));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LeftNavBar);