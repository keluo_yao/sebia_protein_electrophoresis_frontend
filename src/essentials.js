import React, { Component, PropTypes } from 'react';
import { Button } from 'antd';
import { Table } from 'antd';
import { connect } from 'react-redux';
import { processModalLabs } from './actions/data.js';

const { Column, ColumnGroup } = Table;


class Essentials extends Component {


  handleClick = (recordOrderID, localMRN1) => {
    this.props.handleModalAction(recordOrderID, localMRN1)
  }

	render ()
		{
      if (Object.keys(this.props.dataOrderLab).length > 0)
      {
            return (
                <div>
                    <Table dataSource={this.props.dataOrderLab.data.results.OrdersLab2} size="small" pagination={false} rowKey={record => record.OrderID}>
                      <ColumnGroup>
                        <Column
                        title='Order ID'
                        key='OrderID'
                        render={(record) => (
                          <span>
                            <Button type="primary" onClick={()=>this.handleClick(record.OrderID, this.props.dataOrderLab.data.results.Information2[0].MRN)}>
                              <a href="javascript:;">{record.OrderID}</a>
                            </Button>
                          </span>
                        )}
                        />						
                        <Column
                          title='Order Date'
                          dataIndex='OrderDate'
                          key='OrderDate'
                        />
                        <Column
                          title='Name'
                          dataIndex='OrderDisplayName'
                          key='OrderDisplayName'
                          defaultSortOrder='ascend'
                          sorter={(a,b) => a.OrderDisplayName.length - b.OrderDisplayName.length}
                          filters= {[{
                            text: 'TPMT',
                            value: 'TPMT',
                          }]}
                          onFilter={(value, record) => record.OrderDisplayName.indexOf(value) === 0}
                        />
                      </ColumnGroup>
                    
                    </Table>
            </div>
            )
        } else {
          return (
            <div></div>
          )
        }
	}
}

const mapStateToProps = (state) => {
    return {
        // dataOrderLab: state.dataAxiosSuccess.data.results.OrdersLab.Orders,
        dataOrderLab: state.dataAxiosSuccess,
        // localMRN: state.dataAxiosSuccess.data.results.Information.PatientInformation.MRN
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

  return {
      handleModalAction: (recordOrderID, localMRN1) => {
        dispatch(processModalLabs(recordOrderID, localMRN1));
      },
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Essentials);