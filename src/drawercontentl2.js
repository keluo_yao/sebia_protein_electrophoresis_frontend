import React, { Component, PropTypes } from 'react';
import { Drawer, Button } from 'antd';
import { connect } from 'react-redux';
import DrawerContentL3 from './drawercontentl3.js';
import { dataDrawerL2_visibility, dataAxiosSuccess, dataInfoAxiosSuccess, dataMedAxiosSuccess, dataSpepLabResultsAxiosSuccess, dataSpepInterpAxiosSuccess} from './actions/data.js';
import Diagram from './diagram.js';
import RightDisplayContent from './rightdisplaycontent.js';
import Button_Backward_Forward from './button_backward_forward.js';
import { Row, Col } from 'antd';
import Layer_Main_Dashboard_Portal from './layer_main_dashboard_portal.js';
import Layer_Main_Dashbard from './layer_main_dashboard.js';

import { Input } from 'antd';

const { TextArea } = Input;
  
class DrawerContentL2 extends Component {    
    
    handleDrawerClose = () => {
        this.props.drawerClose()
    }

	render () {
        if (Object.keys(this.props.visibleL2.dataDrawerContentL2).length > 0)
		{	

            return (
                <div>
                    <Drawer
                    title={<Layer_Main_Dashboard_Portal />}
                    width="90%"
                    mask={false}
                    closable={true}
                    onClose={()=>this.handleDrawerClose()}
                    visible={this.props.visibleL2.visible}
                    zIndex={2}
                    >
                    <Layer_Main_Dashbard />

                    </Drawer>
                </div>
    
                );
            } else {
                return (
                    <div></div>
                  )
            }

        }
}

const mapStateToProps = (state) => {
    return {
        visibleL2: state.dataDrawerL2,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        drawerClose: () => {
            dispatch(dataDrawerL2_visibility(false));
            // dispatch(dataDrawerL2(false, -1));
            // dispatch(dataAxiosSuccess([]));

        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContentL2);

// export default DrawerContentL1;