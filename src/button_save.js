import React, { Component, PropTypes } from 'react';
import { Button, Icon } from 'antd';
import { connect } from 'react-redux';
import { PgQueryDatatable_insert } from './actions/data.js';




class Button_Save extends Component {


    handleClick = (id, comment) => {
        this.props.handleClickAction(id, comment);
    }


	render () {

        const {id, comment} = this.props

        return (
            <div>
                <Button type="Default" size='large'
                    onClick={()=>this.handleClick(id, comment)}>
                    <Icon type="save" />Save
                </Button>

            </div>
            )
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleClickAction: (id, comment) => {
					dispatch(PgQueryDatatable_insert(id, comment));
        },
    };
};

export default connect(null, mapDispatchToProps)(Button_Save);