import React, { Component, PropTypes } from 'react';
import { Drawer } from 'antd';
import { connect } from 'react-redux';
import History from './history.js';
import { Input, Table } from 'antd';
import LineChart2 from './diagram_linechart_2.js';
import { Card } from "antd";
import { dataDrawerL4_Set_Visibility } from './actions/data.js';
const { Column, ColumnGroup } = Table;

const gridStyle = {
    width: 530,
    textAlign: 'center',
  };

  
class Immunotype extends Component {    
    
    // handleDrawerClose = () => {
    //     this.props.drawerClose()
    // }

    

	render () {

        return (
            <div>
                {this.props.sebiaData &&
                Object.keys(this.props.sebiaData).length > 1 &&
                this.props.sebiaData.slice(2,Object.keys(this.props.sebiaData).length).map(val => (
                    <Card.Grid
                    style={gridStyle}
                    >
                    <LineChart2 data={this.props.sebiaData[1].curva} data2={val.curva} width={450} height={300} margin={10} c_tension={0.5}/>
                    <p><font color="black">{'---' + val.id}</font></p>
                    <p><font color="red">{'---' + 'ELP'}</font></p>
                    </Card.Grid>
                ))}
            </div>
 
            );
        }
}

const mapStateToProps = (state) => {
    return {
        sebiaData: state.sebiaSubtyping,
    };
};

// const mapDispatchToProps = (dispatch, ownProps) => {
//     return {
//         drawerClose: () => {
//             dispatch(dataDrawerL4_Set_Visibility(false));
//         },
//     };
// };

export default connect(mapStateToProps, null)(Immunotype);

// export default DrawerContentL1;