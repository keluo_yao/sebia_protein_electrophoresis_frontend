import React, { Component, PropTypes } from 'react';
import { Button } from 'antd';
import { Table } from 'antd';
import { connect } from 'react-redux';
import { Modal } from 'antd';
import { modalRads } from './actions/data.js';
import { Input } from 'antd';

const { Column, ColumnGroup } = Table;

const { TextArea } = Input;


class Modalrads extends Component {

    handleClick = () => {
        this.props.handleModalAction()
      }

	render ()
		{
      if (Object.keys(this.props.labresultslocal).length > 0)
        {
            return (
                <div>
                    <Modal
                    title={this.props.labresultslocal.data.results.Note.TestResults[0].OrderDisplayName}
                    visible={this.props.modalLabsVisiblelocal}
                    width='70%'
                    onOk={()=>this.handleClick()}
                    onCancel={()=>this.handleClick()}
                    >
                        <TextArea value={this.props.labresultslocal.data.results.Note.TestResults[0].Impression} placeholder="Autosize height based on content lines" autosize />
                        <p>{this.props.labresultslocal.data.results.Note.TestResults[0].ResultDate}</p>
                    </Modal>
            </div>
            )
        } else {
          return (
            <div></div>
          )
        }
	}
}

const mapStateToProps = (state) => {
    return {
        modalLabsVisiblelocal: state.modalRads,
        labresultslocal: state.dataAxiosLabsSuccess,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleModalAction: () => {
          dispatch(modalRads(false));
        },
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(Modalrads);