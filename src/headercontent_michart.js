import React, { Component, PropTypes } from 'react';
import QueryByDateTimeRange_Collection_Info_Only from './querybydatetimerange_collect_info_only.js';
import QueryByMRN_MiChart from './querybymrn_michart.js';



import { Input } from 'antd';
import { Row, Col } from 'antd';

const InputGroup = Input.Group;


class HeaderContent_MiChart extends Component {



	render () {


        return (
            <div>
                    <Row type="flex" justify="start" align="middle">
                        <Col span={6}>
                            <QueryByDateTimeRange_Collection_Info_Only/>
                        </Col>
                        <Col span={4}>
                            <QueryByMRN_MiChart />
                        </Col>

                    </Row>  
            </div>

            )
    }


	
}

export default HeaderContent_MiChart;