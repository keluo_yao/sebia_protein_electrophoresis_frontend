import React, { Component, PropTypes } from 'react';
import { DatePicker } from 'antd';
import { connect } from 'react-redux';
import { dataDrawerL1, min_max_update } from './actions/data.js';

import { Button } from 'antd';
import { Table, Divider, Tag } from 'antd';


const { Column, ColumnGroup } = Table;



class QueryResultTable extends Component {



    handleClick = (record, rowIndex) => {
        this.props.handleDrawerAction(rowIndex, [])
                
	}


	render () {

        return (
            <div>
                    <Table rowKey="queryKey" dataSource={this.props.queryResultTableLocal}
					onRowClick={(record, rowIndex)=>this.handleClick(record, rowIndex)}>
                    <ColumnGroup title="Query Result">					
						<Column
							title="Query Method"
							dataIndex="queryValueString"
							key="queryValueString"
						/>
						<Column
							title="Number of Result(s)"
							dataIndex="queryResultCount"
							key="queryResultCount"
						/>
						</ColumnGroup>
                    </Table>

            </div>
            )
    }
}

const mapStateToProps = (state) => {
    return {
        queryResultTableLocal: state.dataQueryResultTable,
		queryKey: state.updateQueryKey,
		spepDataAllLocal: state.dataSpepPgSuccess,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleDrawerAction: (rowIndex, []) => {
            dispatch(dataDrawerL1(false, rowIndex, []));
            dispatch(min_max_update(0,15)); //need this line to reset main result pagination
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(QueryResultTable);