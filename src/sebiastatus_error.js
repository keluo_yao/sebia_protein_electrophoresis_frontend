import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';
import { Icon } from 'antd';
import { connect } from 'react-redux';

const Search = Input.Search;



class SebiaStatus_Error extends Component {




	render () {
            if (this.props.sebiaStatusError === false) {
                return (
                    <div>
                        <Icon type="check-circle" style={{ fontSize: '25px', color: '#52c41a', verticalAlign: 'middle' }} theme="twoTone" />
                    </div>
                    )
        } else if (this.props.sebiaStatusError === true) {
            return (
                <div>
                    <Icon type="close-circle" style={{ fontSize: '25px', color: '#FF0000', verticalAlign: 'middle' }} theme="twoTone" />
                </div>
            )
        } else {
            <div>
                <Icon type="question-circle" style={{ fontSize: '25px', color: '#FFFF00', verticalAlign: 'middle' }} theme="twoTone" />
        </div>
        }
    }
}

const mapStateToProps = (state) => {
    return {
        sebiaStatusError: state.dataSpepHasErrored,
    };
};


export default connect(mapStateToProps)(SebiaStatus_Error);