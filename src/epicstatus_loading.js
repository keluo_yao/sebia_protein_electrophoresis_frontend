import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';
import { Icon } from 'antd';
import { connect } from 'react-redux';

const Search = Input.Search;



class EpicStatus_Loading extends Component {




	render () {
            if (this.props.epicStatusLoading === false) {
                return (
                    <div>
                        <Icon type="sync" style={{ fontSize: '25px', color: '#696969', verticalAlign: 'middle' }} theme="twoTone" />
                    </div>
                    )
        } else if (this.props.epicStatusLoading === true) {
            return (
                <div>
                    <Icon type="sync" spin style={{ fontSize: '25px', color: '#696969', verticalAlign: 'middle' }} theme="twoTone" />
                </div>
            )
        } else {
            <div>
                <Icon type="question-circle" style={{ fontSize: '25px', color: '#696969', verticalAlign: 'middle' }} theme="twoTone" />
        </div>
        }
    }
}

const mapStateToProps = (state) => {
    return {
        epicStatusLoading: state.dataIsLoading,
    };
};


export default connect(mapStateToProps)(EpicStatus_Loading);