import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Table, Divider, Tag } from 'antd';


const med1 = 'aspirin';

function convertTo2dec (value) {
    return value.toFixed(2);
}



class Speplab extends Component {




	render ()
		{
            return (
                <div>

                    <table width="80%">
                    <tbody>
                    <tr>
                        <th>Name  </th>
                        <th>Percent Fraction  </th>
                        <th>Value  </th>
                    </tr>
                    <tr>
                        <td>Total Protein</td>
                        <td>None</td>
                        <td>{this.props.spepdata.pt}</td>
                    </tr>
                    <tr>
                        <td>Albumin</td>
                        <td>{this.props.spepdata.fraz_1}</td>
                        <td>{convertTo2dec(this.props.spepdata.pt*0.01*this.props.spepdata.fraz_1)}</td>
                    </tr>
                    <tr>
                        <td>Alpha-1</td>
                        <td>{this.props.spepdata.fraz_2}</td>
                        <td>{convertTo2dec(this.props.spepdata.pt*0.01*this.props.spepdata.fraz_2)}</td>
                    </tr>
                    <tr>
                        <td>Alpha-2</td>
                        <td>{this.props.spepdata.fraz_3}</td>
                        <td>{convertTo2dec(this.props.spepdata.pt*0.01*this.props.spepdata.fraz_3)}</td>
                    </tr>
                    <tr>
                        <td>Beta-Globulin</td>
                        <td>{this.props.spepdata.fraz_4}</td>
                        <td>{convertTo2dec(this.props.spepdata.pt*0.01*this.props.spepdata.fraz_4)}</td>
                    </tr>
                    <tr>
                        <td>Gamma Globulin</td>
                        <td>{this.props.spepdata.fraz_5}</td>
                        <td>{convertTo2dec(this.props.spepdata.pt*0.01*this.props.spepdata.fraz_5)}</td>
                    </tr>
                    <tr>
                        <td>Alb/Glob</td>
                        <td>None</td>
                        <td>{convertTo2dec((this.props.spepdata.fraz_1)/(this.props.spepdata.fraz_2+this.props.spepdata.fraz_3+this.props.spepdata.fraz_4+this.props.spepdata.fraz_5))}</td>
                    </tr>
                    <tr>
                        <td>IgG</td>
                        <td>None</td>
                        <td>{this.props.spepdata.free2}</td>
                    </tr>
                    <tr>
                        <td>IgA</td>
                        <td>None</td>
                        <td>{this.props.spepdata.free3}</td>
                    </tr>
                    <tr>
                        <td>IgM</td>
                        <td>None</td>
                        <td>{this.props.spepdata.free4}</td>
                    </tr>
                    <tr>
                        <td>Kappa  Lambda  K/L</td>
                        <td>None</td>
                        <td>{this.props.spepdata.free5}</td>
                    </tr>
                    </tbody>      
                    </table>
                    <p>------------------------------------------------------------</p>
                    <p><th>Significant Medications</th></p>
                    <p>------------------------------------------------------------</p>
                    <p><th>Intepretation</th></p>
                    <p>{this.props.spepdata.commento1}</p>


                </div>
            )
	}
}

const mapStateToProps = (state) => {
    return {
        spepdata: state.dataDrawerL2.dataDrawerContentL2.spepdata,
        epicdata: state.dataAxiosSuccess
    };
};


export default connect(mapStateToProps)(Speplab);