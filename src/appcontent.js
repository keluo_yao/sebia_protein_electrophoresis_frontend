import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Query from './query.js';
import Query_MiChart from './query_michart.js'
import Worklist_Page from './worklist_page.js';
import CaseDisplay from './casedisplay.js';
import LandingPage from './landingpage.js';


class AppContent extends Component {

    render ()
    {
                  if (this.props.leftNavBarSelection === "landingpage") {
                    return <LandingPage />
                  }                  
                  if (this.props.leftNavBarSelection === "logout") {
                    return <div></div>
                  }
                  else if (this.props.leftNavBarSelection === "MiChart") {
                    return <Query_MiChart />
                  }
                  else if (this.props.leftNavBarSelection === "query") {
                    return <Query />
                  }
                  else if (this.props.leftNavBarSelection === "worklist_page") {
                    return <Worklist_Page />
                  }
                  else if (this.props.leftNavBarSelection === "diagram") {
                    return <CaseDisplay />
                  }
                  else {
                    return <div> Error </div>
                  }
  }
}

const mapStateToProps = (state) => {
    return {
        leftNavBarSelection: state.selectLeftNavBar,
    };
};

export default connect(mapStateToProps)(AppContent);