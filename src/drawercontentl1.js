import React, { Component, PropTypes } from 'react';
import { Drawer, Button } from 'antd';
import { connect } from 'react-redux';

import { dataDrawerL1_SetVisibility } from './actions/data.js';
import DrawerContentL2 from './drawercontentl2.js';
import Worklist_Portal from './worklist_portal';
import Worklist from './worklist.js';

  
class DrawerContentL1 extends Component {    
    
    handleDrawerClose = () => {
        this.props.drawerClose()
    }

	render () {

        return (
                <div>
                <Drawer
                title="Worklist"
                width="90%"
                closable={true}
                onClose={()=>this.handleDrawerClose()}
                visible={this.props.drawerContentL1.visible}
                zIndex={1}
                mask={false}
                >
                <Worklist_Portal />
                <Worklist />

                </Drawer>
            </div>
 
            );
        }
}

const mapStateToProps = (state) => {
    return {
        drawerContentL1: state.dataDrawerL1,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        drawerClose: () => {
            dispatch(dataDrawerL1_SetVisibility(false));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContentL1);

// export default DrawerContentL1;