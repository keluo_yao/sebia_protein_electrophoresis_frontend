import React, { Component, PropTypes } from 'react';
import { DatePicker } from 'antd';
import { connect } from 'react-redux';
import { PgQueryDateTimeRange, updateQueryDateTimeRangeValue } from './actions/data.js';


const { RangePicker } = DatePicker;



class QueryByDateTimeRange_Collect_Info_Only extends Component {



    handleOk = (queryDatetimeRangevalue) => {
    this.props.handleClickAction(queryDatetimeRangevalue)
    }


	render () {

        return (
            <div>
                <div>
                    <RangePicker
                    showTime={{ format: 'HH:mm' }}
                    format="YYYY-MM-DD HH:mm"
                    placeholder={['Start Time', 'End Time']}
                    // onChange={value => this.handleOk(value, this.props.queryKey)}
                    onOk={value => this.handleOk(value)}
                    />
				 </div>
            </div>
            )
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleClickAction: (queryDatetimeRangevalue) => {
            dispatch(updateQueryDateTimeRangeValue(queryDatetimeRangevalue));
        },
    };
};

export default connect(null, mapDispatchToProps)(QueryByDateTimeRange_Collect_Info_Only);