import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Diagram from './diagram.js';
import RightDisplayContent from './rightdisplaycontent.js';
import Button_Backward_Forward from './button_backward_forward.js';
import Button_Save from './button_save.js';
import { Row, Col } from 'antd';
import { curve_comment } from './actions/data.js';

import { Input } from 'antd';

const { TextArea } = Input;
  
class Layer_Main_Dashboard extends Component {

    handleUpdate = e => {
        this.props.handleUpdateAction(e.target.value);
        console.log(e.target.value);
    }

	render () {
        if (Object.keys(this.props.visibleL2.dataDrawerContentL2).length > 0)
		{	

            return (
                <div>
                    <Row>
                    <Col span={14}>
                        <Diagram />
                        <b>Comment</b>
                    <TextArea rows={4} value={this.props.comment} onChange={this.handleUpdate} />
                        <Row>
                            <Col span={22}>
                                <Button_Backward_Forward />
                            </Col>
                            <Col span={2}>
                                <Button_Save id={this.props.visibleL2.dataDrawerContentL2.spepdata.id} comment={this.props.comment}/>
                            </Col>
                            
                        </Row>
                        
                    </Col>
                    <Col span={10}>
                        <RightDisplayContent />
                    </Col>
                    </Row>
                </div>
    
                );
            } else {
                return (
                    <div></div>
                  )
            }

        }
}

const mapStateToProps = (state) => {
    return {
        visibleL2: state.dataDrawerL2,
        comment: state.curve_comment
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleUpdateAction: (comment) => {
			dispatch(curve_comment(comment));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Layer_Main_Dashboard);
