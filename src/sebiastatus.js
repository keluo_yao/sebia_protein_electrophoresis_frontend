import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';
import { connect } from 'react-redux';
import { Icon } from 'antd';
import { Row, Col } from 'antd';
import SebiaStatus_Error from './sebiastatus_error.js';
import SebiaStatus_Loading from './sebiastatus_loading.js';
import { Card } from 'antd';




class SebiaStatus extends Component {




	render () {

        return (


            <div>
                <Card bodyStyle={{ padding: 2, background: '#FFFFFF'}} bordered={false}>               
                <Row type="flex" justify="center" align="middle">
                    <Col>
                        <img alt="example" width="100%" src="./img/sebia.png" />
                    </Col>
                    <Col>
                        <div style={{ display: 'inline-flex', justifyContent: 'center', alignItems: 'center', padding: 5}}> 
                            <SebiaStatus_Error />
                        </div>
                    </Col>
                    <Col>
                        <div style={{ display: 'inline-flex', justifyContent: 'center', alignItems: 'center', padding: 5}}> 
                            <SebiaStatus_Loading />
                        </div>
                    </Col>
                </Row>
                </Card> 

            </div>
            )
    }
}


export default SebiaStatus;