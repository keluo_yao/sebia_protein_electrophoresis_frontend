import React, { Component } from 'react';
import { Button, Icon} from 'antd';
import { connect } from 'react-redux';
import moment from 'moment';
import { PgQueryDateTimeRange, updateQueryDateTimeRangeValue, dataDrawerL1 } from './actions/data.js';


class Button_Case_Today extends React.Component {

    handleOk = (querykey2) => {
        let today_start = moment(new Date()).subtract(1, 'days').startOf('day');
        let today_end = moment(new Date()).endOf('day');
        this.props.handleClickAction([today_start, today_end], querykey2);
        }

  render() 
    {

        return (
          <div>
                  <Button type="default" size='default' onClick={() => this.handleOk(this.props.queryKey)}>
                  Today's Cases
                  </Button>
        </div>
        )  
  
    }
  }

  const mapStateToProps = (state) => {

    return {
        queryKey: state.updateQueryKey,
    };
    
  };
  
  const mapDispatchToProps = (dispatch, ownProps) => {
  
    return {
        handleClickAction: (queryDatetimeRangevalue, querykey2) => {
            dispatch(PgQueryDateTimeRange(queryDatetimeRangevalue, querykey2));
            dispatch(updateQueryDateTimeRangeValue(queryDatetimeRangevalue));
            dispatch(dataDrawerL1(false, 0, []));
        },
    };
  };
export default connect(mapStateToProps, mapDispatchToProps)(Button_Case_Today);