import React, { Component, PropTypes } from 'react';
import { Button } from 'antd';
import { Table } from 'antd';
import { Input } from 'antd';
import { connect } from 'react-redux';

import { processModalRads } from './actions/data.js';

const { Column, ColumnGroup } = Table;


const { TextArea } = Input;

const columns = [{
  title: 'Order ID',
  dataIndex: 'OrderID',
  key: 'OrderID',
},{
  title: 'Order Date',
  dataIndex: 'OrderDate',
  key: 'OrderDate',  
},{
  title: 'Name',
  dataIndex: 'OrderDescription',
  key: 'OrderDescription',  
}];

class Radiology extends Component {



  handleClick = (recordOrderID, localMRN1) => {
    this.props.handleModalAction(recordOrderID, localMRN1)
  }
  
  render ()
  {
    if (Object.keys(this.props.dataOrderRad).length > 0)
    {
          return (
              <div>
                  <Table dataSource={this.props.dataOrderRad.data.results.OrdersRad2} size="small" pagination={false}>
                    <ColumnGroup>
                      <Column
                      title='Order ID'
                      key='OrderID'
                      render={(record) => (
                        <span>
                          <Button type="primary" onClick={()=>this.handleClick(record.OrderID, this.props.dataOrderRad.data.results.Information2[0].MRN)}>
                            <a href="javascript:;">{record.OrderID}</a>
                          </Button>
                        </span>
                      )}
                      />						
                      <Column
                        title='Order Date'
                        dataIndex='OrderDate'
                        key='OrderDate'
                      />
                      <Column
                        title='Name'
                        dataIndex='OrderDisplayName'
                        key='OrderDisplayName'
                      />
                    </ColumnGroup>
                  
                  </Table>
          </div>
          )
      } else {
        return (
          <div></div>
        )
      }
}
}

const mapStateToProps = (state) => {
    return {
        dataOrderRad: state.dataAxiosSuccess,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

  return {
      handleModalAction: (recordOrderID, localMRN1) => {
        dispatch(processModalRads(recordOrderID, localMRN1));
      },
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Radiology);