import React, { Component } from 'react';
import { Button, Icon} from 'antd';
import { connect } from 'react-redux';
import { dataDrawerL4_Set_Visibility } from './actions/data.js';


class Button_Immunotype extends React.Component {


  handleClick() {
    this.props.handleClickAction();
  }

  render() 
    {
      if (Object.keys(this.props.sebiaSubtyping).length > 1){
        return (
          <div>
                  <Button type="ghost" size='large' onClick={()=>this.handleClick()}>
                  <Icon type="check-circle" style={{ fontSize: '25px', color: '#52c41a', verticalAlign: 'middle' }} theme="filled" />
                    Immunotyping
                  </Button>
        </div>
        )  

      } else {
        return (
          <div>
                  <Button type="ghost" disabled size='large' onClick={()=>this.handleClick()}>
                  <Icon type="close-circle" style={{ fontSize: '25px', color: '#FF0000', verticalAlign: 'middle' }} theme="twoTone" />
                    Immunotyping
                  </Button>
        </div>
        )  

      }

  
    }
  }

  const mapStateToProps = (state) => {

    return {
      sebiaSubtyping: state.sebiaSubtyping
    };
    
  };
  
  const mapDispatchToProps = (dispatch, ownProps) => {
  
      return {
          handleClickAction: () => {
            dispatch(dataDrawerL4_Set_Visibility(true));
          }

      };
  };
export default connect(mapStateToProps, mapDispatchToProps)(Button_Immunotype);