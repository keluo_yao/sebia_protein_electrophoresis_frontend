import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';
import { Icon } from 'antd';
import { connect } from 'react-redux';

const Search = Input.Search;



class EpicStatus_Error extends Component {




	render () {
            if (this.props.epicStatusError === false) {
                return (
                    <div>
                        <Icon type="check-circle" style={{ fontSize: '25px', color: '#52c41a', verticalAlign: 'middle' }} theme="filled" />
                    </div>
                    )
        } else if (this.props.epicStatusError === true) {
            return (
                <div>
                    <Icon type="close-circle" style={{ fontSize: '25px', color: '#FF0000', verticalAlign: 'middle' }} theme="twoTone" />
                </div>
            )
        } else {
            <div>
                <Icon type="question-circle" style={{ fontSize: '25px', color: '#FFFF00', verticalAlign: 'middle' }} theme="twoTone" />
        </div>
        }
    }
}

const mapStateToProps = (state) => {
    return {
        epicStatusError: state.dataHasErrored,
    };
};


export default connect(mapStateToProps)(EpicStatus_Error);