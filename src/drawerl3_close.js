import { Button } from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { dataDrawerL3 } from './actions/data.js';


 
 
 class Drawerl3_close extends Component {

	handleClick = () => {
		this.props.handleDrawerAction()
	}

 
	 render () {
		
			return (
				 <div>
						<div class="row">
						<div class="column" style={{ padding: '8px 8px 8px 8px'}}>
								<Button type="ghost" size='large' onClick={()=>this.handleClick()}>
									<a href="javascript:;">History</a>
								</Button>
						</div>
              			</div>

				 </div>
				 )
			
	  }
 }


const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleDrawerAction: () => {
			dispatch(dataDrawerL3(true, -1));
        },
    };
};



export default connect(null, mapDispatchToProps)(Drawerl3_close);