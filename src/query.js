import React, { Component, PropTypes } from 'react';
import QueryContent from './querycontent.js';
import HeaderContent from './headercontent.js';
import { Layout, Menu, Icon } from 'antd';
import { Tabs } from 'antd';

const { Header, Footer, Sider, Content } = Layout;
const TabPane = Tabs.TabPane;



class Query extends Component {


	render () {

        return (
            <div>
                                    <Header style={{ background: '#225E8F' }}>
                                      <HeaderContent />
                                    </Header>
                <QueryContent />
            </div>
            )
    }
}

export default Query;