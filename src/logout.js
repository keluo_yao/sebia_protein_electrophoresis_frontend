import React, { Component } from 'react';
import { Layout } from 'antd';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { validateAuthentication } from './actions/data.js';
import './../css/umich.css';

const { Header, Content, Footer } = Layout;
const InputGroup = Input.Group;


const Search = Input.Search;

class Logout extends Component {

  handleEnter = (e) => {
    this.props.handleEnterAction(e.target.value)
  }
	
	render (){
    return (
    <div>

              <p>Enter a password and then hit enter</p>
              <Input type="password" 
              defaultValue="" 
              style={{ width: '200px' }}
              onPressEnter={this.handleEnter}
              />

     </div>
    );
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
      handleEnterAction: (passwordValue) => {
          dispatch(validateAuthentication(passwordValue));
      },
  };
};


export default connect(null, mapDispatchToProps)(Logout);