import React, { Component, PropTypes } from 'react';
import { Button } from 'antd';
import { Table } from 'antd';
import { connect } from 'react-redux';
import { Modal } from 'antd';
import { modalLabs } from './actions/data.js';

const { Column, ColumnGroup } = Table;


class Modallabs extends Component {

    handleClick = () => {
        this.props.handleModalAction()
      }

	render ()
		{
      if (Object.keys(this.props.labresultslocal).length > 0)
        {
            return (
                <div>
                    <Modal
                    title={this.props.labresultslocal.data.results.Note.TestResults[0].OrderDisplayName}
                    visible={this.props.modalLabsVisiblelocal}
                    width='70%'
                    onOk={()=>this.handleClick()}
                    onCancel={()=>this.handleClick()}
                    >
                    <Table dataSource={this.props.labresultslocal.data.results.Note.TestResults[0].Components} size="small" pagination={false}>
                      <ColumnGroup>					
                        <Column
                          title='Name'
                          dataIndex='DisplayName'
                          key='DisplayName'
                        />
                        <Column
                          title='Value'
                          dataIndex='Value'
                          key='Value'
                        />
                        <Column
                          title='ReferenceRange'
                          dataIndex='ReferenceRange'
                          key='ReferenceRange'
                        />
                        <Column
                          title='Comment'
                          dataIndex='Comment'
                          key='Comment'
                          width='30%'
                        />
                      </ColumnGroup>
                    
                    </Table>
                    </Modal>
            </div>
            )
        } else {
          return (
            <div></div>
          )
        }
	}
}

const mapStateToProps = (state) => {
    return {
        modalLabsVisiblelocal: state.modalLabs,
        labresultslocal: state.dataAxiosLabsSuccess,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleModalAction: () => {
          dispatch(modalLabs(false));
        },
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(Modallabs);