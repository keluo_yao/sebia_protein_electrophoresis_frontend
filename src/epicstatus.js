import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';
import { connect } from 'react-redux';
import { Icon } from 'antd';
import { Row, Col } from 'antd';
import EpicStatus_Error from './epicstatus_error.js';
import EpicStatus_Loading from './epicstatus_loading.js';
import { Card } from 'antd';


const Search = Input.Search;



class EpicStatus extends Component {




	render () {

        return (


            <div>
                <Card bodyStyle={{ padding: 2, background: '#FFFFFF'}} bordered={false}>               
                <Row type="flex" justify="center" align="middle">
                    <Col>
                        <img alt="example" width="100%" src="./img/epic.png" />
                    </Col>
                    <Col>
                        <div style={{ display: 'inline-flex', justifyContent: 'center', alignItems: 'center', padding: 5}}> 
                            <EpicStatus_Error />
                        </div>
                    </Col>
                    <Col>
                        <div style={{ display: 'inline-flex', justifyContent: 'center', alignItems: 'center', padding: 5}}> 
                            <EpicStatus_Loading />
                        </div>
                    </Col>
                </Row>
                </Card> 

            </div>
            )
    }
}

const mapStateToProps = (state) => {
    return {
        epicStatusError: state.dataHasErrored,
        epicStatusLoading: state.dataIsLoading
    };
};


export default connect(mapStateToProps)(EpicStatus);