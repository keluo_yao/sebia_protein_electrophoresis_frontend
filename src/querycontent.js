import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Tabs } from 'antd';
import { selectQueryPage } from './actions/data.js';
import QueryResultTable from './queryresulttable.js';
import QueryResult from './queryresult.js';

const TabPane = Tabs.TabPane;

class QueryContent extends Component {

    handleClick = e => {
//        this.props.selectLeftNavBar()
        this.props.handleClickAction(e);
    }



    render ()
		{

        return (
                <div>
                  <Tabs onChange={this.handleClick} type="card">
                    <TabPane tab="Query Result" key="1"><QueryResult /></TabPane>
                    <TabPane tab="Query History" key="2"><QueryResultTable /></TabPane>                                      
                  </Tabs>
                </div>


            )

	}
}

const mapStateToProps = (state) => {
    return {
        queryPage: state.selectQueryPage,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleClickAction: (keySelected) => {
            dispatch(selectQueryPage(keySelected));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(QueryContent);