import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';
import { connect } from 'react-redux';


const columns = [{
  title: 'Name',
  dataIndex: 'Name',
  key: 'Name',
}];

class Medications extends Component {


	render ()
		{
      if (Object.keys(this.props.dataMed).length > 0) {
          return (
            <div>
                <p>All medications recorded in the last 30 days from <b>TODAY</b></p>
                <Table dataSource={this.props.dataMed.data.results.Medications2} columns={columns} size="small" pagination={{ pageSize: 25 }} />
        </div>
        )
      } else {
        return (
          <div>
          </div>
        )
      }
	}
}

const mapStateToProps = (state) => {
    return {
        dataMed: state.dataAxiosSuccess,
    };
};


export default connect(mapStateToProps)(Medications);