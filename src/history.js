import React, { Component } from 'react';
import { connect } from 'react-redux';
import LineChart from './diagram_linechart.js';
import History_Portal from './history_portal.js';
import { Row, Col, Table } from 'antd';
import moment from 'moment';

const { Column, ColumnGroup } = Table;


function convertTo2dec (value) {
  return value.toFixed(2);
}

class History extends Component {



    render ()
		{
            return (
                <div>
                    <History_Portal />

                    <Table dataSource={this.props.dataSpepHistoryLocal}>
                      <ColumnGroup>
                      <Column
                      title="Curve"
                      key="spepdata"
                      render={(record) => (
                          <LineChart data={record.array_sdata} width={350} height={200} margin={10} c_tension={0.5}/>
                      )}
                      />						
                      <Column
                        title="Date and Comment"
                        key="spepdata"
                        render= {(record) => (
                          <div>
                          <p><b>Date </b><b>{moment(record.spepdata.data_analisi).format('MM/DD/YYYY')}</b></p>
                          <p><b>{record.spepdata.commento1}</b></p>
                    </div>

                        )}
                      />
                      <Column
                        title="Values"
                        key="spepdata"
                        render= {(record) => (
                          <div>
                    <table width="80%">
                    <tbody>
                    <tr>
                        <th>Name  </th>
                        <th>Value  </th>
                    </tr>
                    <tr>
                        <td>Total Protein</td>
                        <td>{record.spepdata.pt}</td>
                    </tr>
                    <tr>
                        <td>Albumin</td>
                        <td>{convertTo2dec(record.spepdata.pt*0.01*record.spepdata.fraz_1)}</td>
                    </tr>
                    <tr>
                        <td>Alpha-1</td>
                        <td>{convertTo2dec(record.spepdata.pt*0.01*record.spepdata.fraz_2)}</td>
                    </tr>
                    <tr>
                        <td>Alpha-2</td>
                        <td>{convertTo2dec(record.spepdata.pt*0.01*record.spepdata.fraz_3)}</td>
                    </tr>
                    <tr>
                        <td>Beta-Globulin</td>
                        <td>{convertTo2dec(record.spepdata.pt*0.01*record.spepdata.fraz_4)}</td>
                    </tr>
                    <tr>
                        <td>Gamma Globulin</td>
                        <td>{convertTo2dec(record.spepdata.pt*0.01*record.spepdata.fraz_5)}</td>
                    </tr>
                    <tr>
                        <td>Alb/Glob</td>
                        <td>{convertTo2dec((record.spepdata.fraz_1)/(record.spepdata.fraz_2+record.spepdata.fraz_3+record.spepdata.fraz_4+record.spepdata.fraz_5))}</td>
                    </tr>
                    </tbody>      
                    </table>

                      </div>
                        )}
                      />
                    <Column
                        title="Values"
                        key="spepdata"
                        render= {(record) => (
                          <div>
                          <table width="80%">
                          <tbody>
                            <tr>
                              <th>Name  </th>
                              <th>Value  </th>
                          </tr>

                          <tr>
                              <td>IgG</td>
                              <td>{record.spepdata.free2}</td>
                          </tr>
                          <tr>
                              <td>IgA</td>
                              <td>{record.spepdata.free3}</td>
                          </tr>
                          <tr>
                              <td>IgM</td>
                              <td>{record.spepdata.free4}</td>
                          </tr>
                          <tr>
                              <td>Kappa  Lambda  K/L</td>
                              <td>{record.spepdata.free5}</td>
                          </tr>
                    </tbody>      
                    </table>
                    </div>

                        )}
                      />
                      </ColumnGroup>
					        </Table>
                    
              </div>
              )
   
	}
}

const mapStateToProps = (state) => {

	return {
		 dataSpepHistoryLocal: state.dataSpepHistory,
	};
	
};

export default connect(mapStateToProps)(History);