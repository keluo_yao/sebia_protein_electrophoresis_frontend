import React, { Component } from 'react';
import LineChart from './diagram_linechart.js';

import { connect } from 'react-redux';




class Diagram_small extends React.Component {


  render() 
    {

      return (
        <div>
              <LineChart data={this.props.localSdata} width={600} height={400} margin={25} c_tension={0.5}/>
      </div>
      )    
    }
  }

  const mapStateToProps = (state) => {

    return {
      localSdata: state.dataDrawerL2.dataDrawerContentL2.array_sdata,
    };
    
  };
  
export default connect(mapStateToProps)(Diagram_small);