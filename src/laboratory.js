import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';
import { Input } from 'antd';
import { connect } from 'react-redux';


const { TextArea } = Input;

const columns = [{
  title: 'Name',
  dataIndex: 'Name',
  key: 'Name',
},{
  title: 'ReferenceRange',
  dataIndex: 'ReferenceRange',
  key: 'ReferenceRange',  
},{
  title: 'Value',
  dataIndex: 'Value',
  key: 'Value',  
}];

class Essentials extends Component {


	render ()
		{
            return (
                <div>
                    <Table dataSource={this.props.dataSpepLabResults} columns={columns} size="small" pagination={false} />
                    {this.props.dataSpepInterp[0].Name}
                    <TextArea value={this.props.dataSpepInterp[0].Comment} placeholder="Autosize height based on content lines" autosize />
                    {this.props.dataSpepInterp[1].Name}
                    <TextArea value={this.props.dataSpepInterp[1].Comment} placeholder="Autosize height based on content lines" autosize />
            </div>
            )
	}
}

const mapStateToProps = (state) => {
    return {
        dataLabResults: state.dataAxiosSuccess,
    };
};


export default connect(mapStateToProps)(Laboratory);