import { spepPGSuccess } from "../actions/data";


export function demo_mode(state = {check: false}, action) {
    switch (action.type) {
        case 'DEMO_MODE_CHANGE':
        return {
            check: action.payload.check
        }
        default:
            return state
    }
}

export function min_max(state = {minValue: 0, maxValue: 15}, action ) {
    switch (action.type) {
        case 'MIN_MAX_UPDATE':
            return {
                minValue: action.payload.minValue,
                maxValue: action.payload.maxValue,
            }
        default:
            return state
    }
}


export function user_Authentication_Visibility(state = true, action) {
    switch (action.type) {
        case 'USER_AUTHENTICATION_VISIBILITY':
            return action.authenticated;
        default:
            return state;
    }
}

export function dataSpepHasErrored(state = false, action) {
    switch (action.type) {
        case 'DATA_SPEP_HAS_ERRORED':
            return action.spepHasErrored;
        default:
            return state;
    }
}

export function dataSpepIsLoading(state = false, action) {
    switch (action.type) {
        case 'DATA_SPEP_IS_LOADING':
            return action.spepIsLoading;
        default:
            return state;
    }
}

export function dataSpepPgSuccess(state = [], action) {
    switch (action.type) {
        case 'DATA_SPEP_PG_SUCCESS':
            return [ action.payload, ...state ];
        default:
            return state;
    }
}

export function dataQueryResultTable(state = [], action) {
    switch(action.type) {
        case 'DATA_QUERY_RESULT_TABLE':
            return [ action.payload, ...state ];
        default:
            return state;
    }
}

export function updateQueryKey(state = 0, action) {
    switch(action.type) {
        case 'UPDATE_QUERY_KEY':
            return state + 1;
        default:
            return state;
    }
}

export function updateListKey (state = 0, action) {
    switch(action.type) {
        case 'UPDATE_LIST_KEY':
            return action.listKey;
        default:
            return state;
    }
}

export function updateQueryDateTimeRangeValue (state = [], action) {
    switch(action.type) {
        case 'UPDATE_QUERY_DATE_TIME_RANGE_VALUE':
            return action.queryDateTimeRangeValue;
        default:
            return state;
    }
}

export function updateQueryMRNValue (state = '', action) {
    switch(action.type) {
        case 'UPDATE_QUERY_MRN_VALUE':
            return action.queryMRNValue;
        default:
            return state;
    }
}

export function dataSpepHistory (state = [], action) {
    switch(action.type) {
        case 'DATA_SPEP_HISTORY':
            return action.spepHistory;
        default:
            return state;
    }
}

export function dataDrawerL1(state = {visible: false, queryKey: 0, drawerContentL1: []}, action) {
    switch (action.type) {
        case 'DATA_DRAWER_L1':
            // return action.payload;
            return {
                visible: action.payload.visible,
                queryKey: action.payload.queryKey,
                drawerContent: action.payload.drawerContentL1.filter(function(spep) { return (spep.queryKey === action.payload.queryKey)}),
            }
        case 'DATADRAWERL1_SET_VISIBILITY':
            return {
                ...state, visible: action.payload.visible
            }
        default:
            return state;
    }
}

export function sebiaSubtyping(state = [], action) {
    switch (action.type) {
        case 'SEBIA_SUBTYPING_NEW':
            return action.payload.sebiaData;
    
        case 'SEBIA_SUBTYPING_ADD':
            return [
                    action.payload, ...state
            ];
        case 'SEBIA_SUBTYPING_CLEAR':
            return [
                {}
            ];
        default:
            return state;

    }
}

export function dataDrawerL2(state = {visible: false, dataDrawerContentL2: []}, action) {
    switch (action.type) {
        case 'DATA_DRAWER_L2':
            return action.payload;
        case 'DATADRAWERL2_VISIBILITY':
            return {
                ...state, visible: action.payload.visibility
            }
        default:
            return state;
    }
}

export function curve_comment(state = '', action) {
    switch(action.type) {
        case 'CURVE_COMMENT':
            return action.curveComment;
        default:
            return state;
    }
}

export function curve_width(state = 1000, action) {
    switch(action.type) {
        case 'CURVE_WIDTH':
            return action.curveWidth;
        default:
            return state;
    }
}

export function curve_height(state = 600, action) {
    switch(action.type) {
        case 'CURVE_HEIGHT':
            return action.curveHeight;
        default:
            return state;
    }
}

export function curve_c_tension(state = 0.5, action) {
    switch(action.type) {
        case 'CURVE_C_TENSION':
            return action.curveCTension;
        default:
            return state;
    }
}

export function curve_margin(state = 50, action) {
    switch(action.type) {
        case 'CURVE_MARGIN':
            return action.curveMargin;
        default:
            return state;
    }
}

export function dataDrawerL3(state = {visible: false, drawerContent: -1}, action) {
    switch (action.type) {
        case 'DATA_DRAWER_L3':
            return action.payload;
        default:
            return state;
    }
}

export function dataDrawerL4(state = {visibility: false}, action) {
    switch (action.type) {
        case 'DATA_DRAWER_L4_SET_VISIBILITY':
            return action.payload;
        default: 
            return state;
    }
}

export function dataDrawerType(state = 'single', action) {
    switch (action.type) {
        case 'DATA_DRAWER_TYPE':
            return action.drawerType;
        default:
            return state;
    }
}

export function dataDrawerCargoSpep(state = {}, action) {
    switch (action.type) {
        case 'DATA_DRAWER_CARGE_SPEP':
            return action.drawerCargoSpep;
        default:
            return state;
    }
}

export function modalLabs(state = false, action) {
    switch (action.type) {
        case 'MODAL_LABS':
            return action.modalLabsVisible;
        default:
            return state;
    }
}

export function modalRads(state = false, action) {
    switch (action.type) {
        case 'MODAL_RADS':
            return action.modalRadsVisible;
        default:
            return state;
    }
}

export function modalNotes(state = false, action) {
    switch (action.type) {
        case 'MODAL_NOTES':
            return action.modalNotesVisible;
        default:
            return state;
    }
}


export function dataHasErrored(state = false, action) {
	switch (action.type) {
		case 'DATA_HAS_ERRORED':
			return action.hasErrored;
		default:
			return state;
	}
}

export function dataIsLoading(state = false, action) {
    switch (action.type) {
        case 'DATA_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function dataAxiosSuccess(state = {}, action) {
    switch (action.type) {
        case 'DATA_AXIOS_SUCCESS':
            return action.dataValue;

        default:
            return state;
    }
}

export function dataAxiosLabsSuccess(state = {}, action) {
    switch (action.type) {
        case 'DATA_AXIOS_LABS_SUCCESS':
            return action.dataLabsValue;
        default:
            return state;
    }
}

export function dataInfoAxiosSuccess(state = [], action) {
    switch (action.type) {
        case 'DATA_AXIOS_INFO_SUCCESS':
            return action.dataValueInfo;
        default:
            return state;
    }
}

export function dataMedAxiosSuccess(state = [], action) {
    switch (action.type) {
        case 'DATA_MED_AXIOS_SUCCESS':
            return action.dataMed;
        default:
            return state;
    }
}

export function dataSpepLabResultsAxiosSuccess(state = [], action) {
    switch (action.type) {
        case 'DATA_SPEPLABRESULTS_AXIOS_SUCCESS':
            return action.dataSpepLabResults;
        default:
            return state;
    }
}

export function dataSpepInterpAxiosSuccess(state = [{key: "0", Name: "SPEP Interpretation", Comment: ""}, {key: "1", Name: "IMMUNOFIXATION", Comment: ""}], action) {
    switch (action.type) {
        case 'DATA_SPEP_INTERP_AXIOS_SUCCESS':
            return action.dataSpepInterp;
        default:
            return state;
    }
}

export function dataSebiaPGSuccess(state = {}, action)  {
    switch (action.type) {
        case 'DATA_SEBIA_PG_SUCCESS':
            return action.dataSebia;
        default:
            return state;
    }
}

export function dataOrderLabsSuccess(state = [], action) {
    switch (action.type) {
        case 'DATA_ORDERS_LABS_SUCCESS':
            return action.dataOrderLabs;
        default:
            return state;
    }
}

export function dataNotesSuccess(state = [], action) {
    switch (action.type) {
        case 'DATA_NOTES_SUCCESS':
            return action.dataNotes;
        default:
            return state;
    }
}


export function selectLeftNavBar(state = 'query', action) {
    switch (action.type) {
        case 'SELECT_LEFT_NAV_BAR':
            return action.leftNavBarSelection;
        default:
            return state;
    }
}

export function selectRightNavBar(state = '1', action) {
    switch (action.type) {
        case 'SELECT_RIGHT_NAV_BAR':
            return action.rightNavBarSelection;
        default:
            return state;
    }
}


export function selectQueryPage(state ='1', action) {
    switch (action.type) {
        case 'SELECT_QUERY_PAGE':
            return action.queryPage;
        default:
            return state;
    }
}

export function processSearchValue(state = 'fake value', action)  {
    switch (action.type) {
        case 'PROCESS_SEARCH_VALUE':
            return action.searchValue;
        default:
            return state;
    }
}