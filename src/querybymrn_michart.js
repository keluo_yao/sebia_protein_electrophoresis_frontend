import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';
import { connect } from 'react-redux';
import { updateQueryMRNValue } from './actions/data.js';
import { PgQueryMRN } from './actions/data.js';
import { dataAxiosFetch } from './actions/data.js';
import moment from 'moment';

const Search = Input.Search;



class QueryByMRN_MiChart extends Component {



    handleOk = (queryMRNvaluelocal, LocalDateTimeRange) => {
    this.props.handleClickAction(queryMRNvaluelocal, LocalDateTimeRange)
    }


	render () {

        return (
            <div>
                <div>
                    <Search
                    placeholder="Enter MRN"
                    onSearch={value => this.handleOk(value, this.props.localDateTimeRange)}
                    style={{ width: 200 }}
                    />
				 </div>
            </div>
            )
    }
}

const mapStateToProps = (state) => {
    return {
        localDateTimeRange: state.updateQueryDateTimeRangeValue
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleClickAction: (queryMRNvaluelocal, localDateTimeRange) => {
            dispatch(dataAxiosFetch(queryMRNvaluelocal, localDateTimeRange[0].format('MM/DD/YYYY'), localDateTimeRange[1].format('MM/DD/YYYY')));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(QueryByMRN_MiChart);