import React, { Component, PropTypes } from 'react';
import { DatePicker } from 'antd';
import { connect } from 'react-redux';
import { PgQueryDateTimeRange, updateQueryDateTimeRangeValue, dataDrawerL1 } from './actions/data.js';


const { RangePicker } = DatePicker;



class QueryByDateTimeRange extends Component {



    handleOk = (queryDatetimeRangevalue, querykey2) => {
    this.props.handleClickAction(queryDatetimeRangevalue, querykey2)
    }


	render () {

        return (
            <div>
                <div>
                    <RangePicker
                    showTime={{ format: 'HH:mm' }}
                    format="YYYY-MM-DD HH:mm"
                    placeholder={['Start Time', 'End Time']}
                    onOk={value => this.handleOk(value, this.props.queryKey)}
                    />
				 </div>
            </div>
            )
    }
}

const mapStateToProps = (state) => {
    return {
        queryKey: state.updateQueryKey,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleClickAction: (queryDatetimeRangevalue, querykey2) => {
            dispatch(PgQueryDateTimeRange(queryDatetimeRangevalue, querykey2));
            dispatch(updateQueryDateTimeRangeValue(queryDatetimeRangevalue));
            dispatch(dataDrawerL1(false, 0, []));        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(QueryByDateTimeRange);