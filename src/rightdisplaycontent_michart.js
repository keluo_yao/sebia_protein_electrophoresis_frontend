import React, { Component } from 'react';
import PatientInfo from './patientinfo.js';
import RightNavBar_MiChart from './rightnavbar_michart.js';
import Modallabs from './modallabs.js';
import Modalnotes from './modalnotes.js';
import Modalrads from './modalrads.js';
import Speplab from './speplab.js';

class RightDisplayContent_MiChart extends Component {

    render ()
    {


                    return (
                    		<div>
                    		    <PatientInfo />

                    			<RightNavBar_MiChart />
                                <Modallabs />
                                <Modalnotes />
                                <Modalrads />
                            </div>
                      )
    }

}

export default RightDisplayContent_MiChart;