import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal } from 'antd';
import Logout from './logout.js';

class User_Authentication extends Component {

    handleClick = () => {

      }

    render ()
		{
            return (
              <div>
                    <Modal
                    title={'Warning, software in alpha phase, not for clinical use!'}
                    visible={this.props.authentication_status_local}
                    onOk={()=>this.handleClick()}
                    onCancel={()=>this.handleClick()}
                    >
                    <Logout />
                    </Modal>
              </div>
              )
   
	}
}

const mapStateToProps = (state) => {
    return {
        authentication_status_local: state.user_Authentication_Visibility,
    };
};

export default connect(mapStateToProps)(User_Authentication);