import React, { Component } from 'react';
import Diagram from './diagram.js';
import RightDisplayContent from './rightdisplaycontent.js';
import { Row, Col } from 'antd';

class CaseDisplay extends Component {

    render ()
		{
            return (
              <div>
                <Row>
                  <Col span={14}>
                    <Diagram />
                  </Col>
                  <Col span={10}>
                    <RightDisplayContent />
                  </Col>
                </Row>
              </div>
              )
   
	}
}

export default CaseDisplay;