import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';
import { connect } from 'react-redux';
import { updateQueryMRNValue } from './actions/data.js';
import { PgQueryMRN } from './actions/data.js';

const Search = Input.Search;



class QueryByMRN extends Component {



    handleOk = (queryMRNvaluelocal, querykey2) => {
    this.props.handleClickAction(queryMRNvaluelocal, querykey2)
    }


	render () {

        return (
            <div>
                <div>
                    <Search
                    placeholder="Enter MRN"
                    onSearch={value => this.handleOk(value, this.props.queryKey)}
                    style={{ width: 200 }}
                    />
				 </div>
            </div>
            )
    }
}

const mapStateToProps = (state) => {
    return {
        queryKey: state.updateQueryKey,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleClickAction: (queryMRNvaluelocal, querykey2) => {
            dispatch(updateQueryMRNValue(queryMRNvaluelocal));
            dispatch(PgQueryMRN(queryMRNvaluelocal, querykey2));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(QueryByMRN);