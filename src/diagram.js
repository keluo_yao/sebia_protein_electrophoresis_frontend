import React, { Component } from 'react';
import LineChart from './diagram_linechart_w_marker.js';
import History_Portal from './history_portal.js';
import Drawerl3_close from './drawerl3_close.js';
import Button_Immunotype from './button_immunotype.js';

import { Slider, InputNumber, Button, Radio, Icon, Pagination } from 'antd';
import { Row, Col } from 'antd';
import { connect } from 'react-redux';
import { curve_width, curve_height, curve_c_tension } from './actions/data.js';
import { Input } from 'antd';
const { TextArea } = Input;


class Diagram extends React.Component {


  handleLargerDiagram() {
    this.props.handleLargerDiagram_1(this.props.localWidth*1.1, this.props.localHeight*1.1);
  }

  handleSmallerDiagram() {
    this.props.handleSmallerDiagram_1(this.props.localWidth/1.1, this.props.localHeight/1.1);
  }

  handleC_TensionChange = (value) => {
    this.props.handleC_TensionChange_1(value);
  }

  render() 
    {

      return (
        <div>
          <Row>
            <Col span={6}>
            <Drawerl3_close />
            </Col>
            <Col span={6}>
              <div class="row">
              <div class="column" style={{ padding: '8px 8px 8px 8px'}}>
                <Button.Group>
                  <Button type="ghost" size='large' onClick={()=>this.handleLargerDiagram()}><Icon type="plus" />Larger</Button>
                  <Button type="ghost" size='large' onClick={()=>this.handleSmallerDiagram()}><Icon type="minus"/>Smaller</Button>
                </Button.Group>
                <p>Make waveform larger or smaller</p>
              </div>
              </div>
              </Col>

              <Col span={6}>
              <div class="row">
              <div class="column" style={{ padding: '8px 8px 8px 8px'}}>
                  <Button_Immunotype />
                  </div>
              </div>
              </Col>
              <Col span={6}>
              <div class="row">
              <div class="column" style={{ padding: '8px 8px 8px 8px'}}>
                <Slider min={0} max={1} onChange={this.handleC_TensionChange} Value={this.props.localCTension} step={0.01} />
                <p>Use slider to adjust smoothness</p>
                </div>
              </div>
              </Col>           
            <Col span={24}>
              <LineChart data={this.props.localSdata} marker={this.props.localSmarker} width={this.props.localWidth} height={this.props.localHeight} margin={this.props.localMargin} c_tension={this.props.localCTension}/>
            </Col>
          </Row>
      </div>
      )    
    }
  }

  const mapStateToProps = (state) => {

    return {
      localWidth: state.curve_width,
      localHeight: state.curve_height,
      localCTension: state.curve_c_tension,
      localMargin: state.curve_margin,
      localSdata: state.dataDrawerL2.dataDrawerContentL2.array_sdata,
      localSmarker: state.dataDrawerL2.dataDrawerContentL2.marker_sdata,
    };
    
  };
  
  const mapDispatchToProps = (dispatch, ownProps) => {
  
      return {
          handleLargerDiagram_1: (localWidth1, localHeight1) => {
            dispatch(curve_width(localWidth1));
            dispatch(curve_height(localHeight1));
          },
          handleSmallerDiagram_1: (localWidth1, localHeight1) => {
            dispatch(curve_width(localWidth1));
            dispatch(curve_height(localHeight1));
          },
          handleC_TensionChange_1: (value1) => {
            dispatch(curve_c_tension(value1));
          }

      };
  };
export default connect(mapStateToProps, mapDispatchToProps)(Diagram);