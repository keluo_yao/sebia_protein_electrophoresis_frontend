import React, { Component, PropTypes } from 'react';
import { Tabs } from 'antd';
import { connect } from 'react-redux';
import { Card, Pagination } from "antd";
import { Row, Col } from 'antd';
import LineChart from './diagram_linechart.js';
import { dataDrawerL2_DataMassage, sebiaSubtyping_Clear, min_max_update } from './actions/data.js';


const TabPane = Tabs.TabPane;

const gridStyle = {
    width: 310,
    height: 250,
    textAlign: 'center',
  };


class QueryResult_Show extends Component {


      handleChange = value => {
        if (value <= 1) {
          this.props.handleMinMax(0,15);
        } else if  ((value -1 ) * 15 + 15  > Object.keys(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue).length) {
          this.props.handleMinMax((value - 1) * 15, Object.keys(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue).length)
        } else {
          this.props.handleMinMax((value -1) * 15, (value -1 ) * 15 + 15)
        }
      };

      handleClick = (drawerContentLocalL2, localsdata, key) => {
        this.props.handleDrawerAction(drawerContentLocalL2, localsdata, key);
    }


    render ()
		{
            if (this.props.checklocal.check === false) {
                return (
                    <div>
                        <Row>
                            <Col span={24}>
                            <p><font color="black">Displaying {Object.keys(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue).length} Result(s)</font></p>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                    {this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue &&
                                Object.keys(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue).length > 0 &&
                                this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue.slice(this.props.min_max.minValue, this.props.min_max.maxValue).map((val, index) => (
                                  
                                    <Card.Grid
                                    style={gridStyle}
                                    onClick={()=>this.handleClick(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue[this.props.min_max.minValue + index], this.props.min_max.minValue + index)}
                                    >
                                    <LineChart data={val.array_sdata} width={250} height={150} margin={0} c_tension={0.5}/>
                                    <p class ="a"><font color="black">
                                    <b>
                                        Name: </b>{val.spepdata.nominativo}
                                        </font></p>
                                    <p class = "a"><font color="black"><b>MRN: </b>{val.spepdata.free1}</font></p>
                                    <p class = "a"><font color="black"><b>Barcode: </b>{val.spepdata.id}</font></p>
                                    </Card.Grid>
                                ))}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>

                                <Pagination
                                defaultCurrent={1}
                                defaultPageSize={15}
                                onChange={this.handleChange}
                                total={Object.keys(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue).length}
                            />
                            </Col>
                        </Row>





                    </div>    
                )
            } else {

                return (
                    <div>
                        <Row>
                            <Col span={24}>
                            <p><font color="black">Displaying {Object.keys(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue).length} Result(s)</font></p>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                    {this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue &&
                                Object.keys(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue).length > 0 &&
                                this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue.slice(this.props.min_max.minValue, this.props.min_max.maxValue).map((val, index) => (
                                  
                                    <Card.Grid
                                    style={gridStyle}
                                    onClick={()=>this.handleClick(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue[this.props.min_max.minValue + index], this.props.min_max.minValue + index)}
                                    >
                                    <LineChart data={val.array_sdata} width={250} height={150} margin={0} c_tension={0.5}/>
                                    <p class ="a"><font color="black">
                                    <b>
                                        Name: </b>Jane Doe
                                        </font></p>
                                    <p class = "a"><font color="black"><b>MRN: </b>123456789</font></p>
                                    <p class = "a"><font color="black"><b>Barcode: </b>1234567890</font></p>
                                    </Card.Grid>
                                ))}
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>

                                <Pagination
                                defaultCurrent={1}
                                defaultPageSize={15}
                                onChange={this.handleChange}
                                total={Object.keys(this.props.queryResultLocal[this.props.queryKeyLocal].spepPgValue).length}
                            />
                            </Col>
                        </Row>





                    </div>    
                )
            }


	}
}

const mapStateToProps = (state) => {
    return {
        queryResultLocal: state.dataSpepPgSuccess,
        queryKeyLocal: state.dataDrawerL1.queryKey,
        min_max: state.min_max,
        checklocal: state.demo_mode,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleDrawerAction: (drawerContentLocalL2, curvedata, key) => {
					dispatch(sebiaSubtyping_Clear());
					dispatch(dataDrawerL2_DataMassage(drawerContentLocalL2, curvedata, key));
        },
        handleMinMax: (minValue, maxValue) => {
          dispatch(min_max_update(minValue,maxValue))
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(QueryResult_Show);