import React, { Component } from 'react';
import WindowPortal_1 from './windowportal_1.js';
import Worklist from './worklist.js';
import { Button } from 'antd';
import { dataDrawerL1_SetVisibility } from './actions/data.js';
import { connect } from 'react-redux';

class Workslist_Portal extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
          counter: 0,
          showWindowPortal: false,
        };
        
        this.toggleWindowPortal = this.toggleWindowPortal.bind(this);
        this.closeWindowPortal = this.closeWindowPortal.bind(this);
      }
    
      componentDidMount() {
        window.addEventListener('beforeunload', () => {
          this.closeWindowPortal();
        });
        
        window.setInterval(() => {
          this.setState(state => ({
            counter: state.counter + 1,
          }));
        }, 1000);
      }
      
      toggleWindowPortal() {
        this.setState(state => ({
          ...state,
          showWindowPortal: !state.showWindowPortal,
        }));
        this.props.drawerClose();
        
      }
      
      closeWindowPortal() {
        this.setState({ showWindowPortal: false })
      }

    render ()
		{
            return (
                <div>
                {/* <h1>Counter: {this.state.counter}</h1> */}
                
                <Button onClick={this.toggleWindowPortal}>
                  {this.state.showWindowPortal ? 'Close the' : 'Open a'} Worklist Portal
                </Button>
                
                {this.state.showWindowPortal && (
                  <WindowPortal_1 closeWindowPortal={this.closeWindowPortal} >
                    {/* <h1>Counter in a portal: {this.state.counter}</h1>
                    <p>Even though I render in a different window, I share state!</p> */}
                <Worklist />
                    
                    <button onClick={() => this.closeWindowPortal()} >
                      Close Worklist Portal
                    </button>
                  </WindowPortal_1>
                )}
              </div>
              )
   
	}
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
      drawerClose: () => {
          dispatch(dataDrawerL1_SetVisibility(false));
      },
  };
};

export default connect(null, mapDispatchToProps)(Workslist_Portal);