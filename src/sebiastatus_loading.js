import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';
import { Icon } from 'antd';
import { connect } from 'react-redux';



class SebiaStatus_Loading extends Component {




	render () {
            if (this.props.sebiaStatusLoading === false) {
                return (
                    <div>
                        <Icon type="sync" style={{ fontSize: '25px', color: '#696969', verticalAlign: 'middle' }} theme="twoTone" />
                    </div>
                    )
        } else if (this.props.sebiaStatusLoading === true) {
            return (
                <div>
                    <Icon type="sync" spin style={{ fontSize: '25px', color: '#696969', verticalAlign: 'middle' }} theme="twoTone" />
                </div>
            )
        } else {
            <div>
                <Icon type="question-circle" style={{ fontSize: '25px', color: '#696969', verticalAlign: 'middle' }} theme="twoTone" />
        </div>
        }
    }
}

const mapStateToProps = (state) => {
    return {
        sebiaStatusLoading: state.dataSpepIsLoading,
    };
};


export default connect(mapStateToProps)(SebiaStatus_Loading);