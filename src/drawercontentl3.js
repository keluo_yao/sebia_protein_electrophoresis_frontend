import React, { Component, PropTypes } from 'react';
import { Drawer, Button } from 'antd';
import { connect } from 'react-redux';
import History from './history.js';
import { Input, Table } from 'antd';
import { dataDrawerL3 } from './actions/data.js';
const { Column, ColumnGroup } = Table;

  
class DrawerContentL3 extends Component {    
    
    handleDrawerClose = () => {
        this.props.drawerClose()
    }

	render () {

        return (
            <div>
                <Drawer
                title="Dashboard"
                width="90%"
                closable={true}
                onClose={()=>this.handleDrawerClose()}
                visible={this.props.visibleL3.visible}
                zIndex={3}
                >

                <History />

                </Drawer>
            </div>
 
            );
        }
}

const mapStateToProps = (state) => {
    return {
        visibleL3: state.dataDrawerL3,
        dataSpepHistoryLocal: state.dataSpepHistory,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        drawerClose: () => {
            dispatch(dataDrawerL3(false, -1));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContentL3);

// export default DrawerContentL1;