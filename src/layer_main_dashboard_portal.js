import React, { Component } from 'react';

import WindowPortal_1 from './windowportal_1.js';
import Layer_Main_Dashboard from './layer_main_dashboard.js';
import { Button } from 'antd';

class Layer_Main_Dashboard_Portal extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
          counter: 0,
          showWindowPortal: false,
        };
        
        this.toggleWindowPortal = this.toggleWindowPortal.bind(this);
        this.closeWindowPortal = this.closeWindowPortal.bind(this);
      }
    
      componentDidMount() {
        window.addEventListener('beforeunload', () => {
          this.closeWindowPortal();
        });
        
        window.setInterval(() => {
          this.setState(state => ({
            counter: state.counter + 1,
          }));
        }, 1000);
      }
      
      toggleWindowPortal() {
        this.setState(state => ({
          ...state,
          showWindowPortal: !state.showWindowPortal,
        }));
      }
      
      closeWindowPortal() {
        this.setState({ showWindowPortal: false })
      }

    render ()
		{
            return (
                <div>
                {/* <h1>Counter: {this.state.counter}</h1> */}
                
                <Button onClick={this.toggleWindowPortal}>
                  {this.state.showWindowPortal ? 'Close the' : 'Open a'} Portal
                </Button>
                
                {this.state.showWindowPortal && (
                  <WindowPortal_1 closeWindowPortal={this.closeWindowPortal} >

                        <Layer_Main_Dashboard />
                    
                    <button onClick={() => this.closeWindowPortal()} >
                      Close me!
                    </button>
                  </WindowPortal_1>
                )}
              </div>
              )
   
	}
}


export default Layer_Main_Dashboard_Portal;