import React, { Component, PropTypes } from 'react';
import QueryByDateTimeRange from './querybydatetimerange.js';
import QueryByMRN from './querybymrn.js';
import Button_Case_Today from './button_case_today.js';
import Layer_Query_Portal from './layer_query_portal.js';



import { Input } from 'antd';
import { Row, Col } from 'antd';

const InputGroup = Input.Group;


class HeaderContent extends Component {



	render () {


        return (
            <div>
                    <Row type="flex" justify="start" align="middle">
                        <Col span={2}>
                            <Layer_Query_Portal />
                        </Col>
                        <Col span={6}>
                            <QueryByDateTimeRange />
                        </Col>
                        <Col span={4}>
                            <QueryByMRN />
                        </Col>
                        <Col span={2}>
                            <Button_Case_Today />
                        </Col>

                    </Row>  
            </div>

            )
    }


	
}

export default HeaderContent;