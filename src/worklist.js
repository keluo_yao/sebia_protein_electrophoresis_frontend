import { Button } from 'antd';
import React, { Component } from 'react';
import { Table, Divider, Tag } from 'antd';
import { connect } from 'react-redux';
import moment from 'moment';
import { dataDrawerL2, dataDrawerL2_DataMassage, sebiaSubtyping_Clear } from './actions/data.js';


const { Column, ColumnGroup } = Table;
 
 
 class Worklist extends Component {

	handleClick = (drawerContentLocalL2, localDateTimeRange1) => {
		        this.props.handleDrawerAction(drawerContentLocalL2, localDateTimeRange1)
	}

	handleDateFormat = (recieveddata) => {
		switch (recieveddata) {
			case 'Invalid date':
				return '';
			default:
				return recieveddata;

		}

	}

 
	 render () {

		if (Object.keys(this.props.DrawerContentLocalL1.drawerContent).length > 0)
		{			
			return (
				 <div>
					<Table dataSource={this.props.DrawerContentLocalL1.drawerContent[0].spepPgValue}
					onRowClick={(record, rowIndex)=>console.log(record, rowIndex)}
					>
						<ColumnGroup>
						<Column
						title="Patient Name"
						key="nominativo"
						sorter="true"
						render={(record) => (
							<span>
								<Button type="ghost" onClick={()=>this.handleClick(record, this.props.localDateTimeRange)}>
									<a href="javascript:;">{record.nominativo}</a>

								</Button>
							</span>
						)}
						/>						
						<Column
							title="Date and Time"
							key="data_analisi"
							render= {(record) => (
								<span>
									{moment(record.data_analisi).format('MM/DD/YYYY hh:mm:ss a')}
								</span>
							)}
						/>
						<Column
							title="MRN"
							dataIndex="free1"
							key="free1"
						/>
						<Column
							title="ID"
							dataIndex="id"
							key="id"
						/>
						<Column
						title="Age"
						dataIndex="eta"
						key="eta"
						/>
						<Column
						title="Date of Birth"
						key="data_nascita"
						render= {(record) => (
							<span>
								{this.handleDateFormat(moment(record.data_nascita).format('MM/DD/YYYY'))}
							</span>
							)}
						/>
						</ColumnGroup>
					</Table>
				 </div>
				 )
			} else {
				return (
				  <div></div>
				)
			}
	  }
 }

const mapStateToProps = (state) => {

	return {
		 DrawerContentLocalL1: state.dataDrawerL1,
		 localDateTimeRange: state.updateQueryDateTimeRangeValue
	};
	
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        handleDrawerAction: (drawerContentLocalL2, localDateTimeRange1) => {
					dispatch(sebiaSubtyping_Clear());
					dispatch(dataDrawerL2_DataMassage(drawerContentLocalL2, localDateTimeRange1));
			// dispatch(dataDrawerL2(true, drawerContentLocalL2));
        },
    };
};



export default connect(mapStateToProps, mapDispatchToProps)(Worklist);