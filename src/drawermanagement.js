import React, { Component, PropTypes } from 'react';
import DrawerContentL1 from './drawercontentl1.js';
import DrawerContentL2 from './drawercontentl2.js';
import DrawerContentL3 from './drawercontentl3.js';
import DrawerContentL4 from './drawercontentl4.js';



class DrawerManagement extends Component {


	render () {

        return (
            <div>
                <DrawerContentL1 />
                <DrawerContentL2 />                    
                <DrawerContentL3 />
                <DrawerContentL4 />
            </div>
            )
    }
}

export default DrawerManagement;