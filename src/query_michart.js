import React, { Component, PropTypes } from 'react';
import DrawerContentL1 from './drawercontentl1.js';
import QueryByDateTimeRange from './querybydatetimerange.js';
import QueryResultTable from './queryresulttable.js';
import HeaderContent_MiChart from './headercontent_michart.js';
import { Layout, Menu, Icon } from 'antd';
import RightDisplayContent_MiChart from './rightdisplaycontent_michart.js';

const { Header, Footer, Sider, Content } = Layout;



class Query_MiChart extends Component {


	render () {

        return (
            <div>
                                    <Header style={{ background: '#225E8F' }}>
                                      <HeaderContent_MiChart />
                                    </Header>
                                    <RightDisplayContent_MiChart />


            </div>
            )
    }
}

export default Query_MiChart;