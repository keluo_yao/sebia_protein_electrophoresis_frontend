import axios from 'axios';
import moment from 'moment';



const urlEpic = 'http://###########:3000/spep/api/v1/results';
const urlLabs = 'http://##########:3000/spep/api/v1/orderresults';
const urlNotes = 'http://############:3000/spep/api/v1/notes';
const urlSebia = 'http://###########:3000/spep/api/v1/SPEP';
const urlSebia_MRN = 'http://#########:3000/spep/api/v1/SPEP_MRN';
const urlSebia_ID_M = 'http://#########:3000/spep/api/v1/SPEP_ID_MORE';
const urlDatatable_View = 'http://##########:3000/spep/api/v1/commentview';
const urlDatatable_Insert = 'http://###########:3000/spep/api/v1/commentinsert';




//handles demo mode setting

export function demo_mode_change(check) {
    return {
        type: 'DEMO_MODE_CHANGE',
        payload: {
            check
        }
    }
}

//handles main result page

export function min_max_update(minValue, maxValue) {
    return {
        type: 'MIN_MAX_UPDATE',
        payload: {
            minValue,
            maxValue
        }
    }
}

//handles user authentication

export function user_Authentication_Visibility(bool) {
    return {
        type: 'USER_AUTHENTICATION_VISIBILITY',
        authenticated: bool
    }
}

export function validateAuthentication(passwordValue) {
    console.log(passwordValue);
    if (passwordValue === 'SPEPAlpha') {
        console.log('pass auth');
        return (dispatch) => {
            dispatch(user_Authentication_Visibility(false));
        } 
            } else {
                console.log('fail auth');
                return (dispatch) => {

            dispatch(user_Authentication_Visibility(true));
                }
        }
}

//handles pg request for spep
export function dataSpepHasErrored(bool) {
    return {
        type: 'DATA_SPEP_HAS_ERRORED',
        spepHasErrored: bool
    }
}

export function dataSpepIsLoading(bool) {
    return {
        type: 'DATA_SPEP_IS_LOADING',
        spepIsLoading: bool
    }
}

export function dataSpepPgSuccess(queryKey, queryValue, spepPgValue) {
    return {
        type: 'DATA_SPEP_PG_SUCCESS',
        payload: {
            queryKey,
            queryValue,
            spepPgValue
        }
    }
}

//handle the data table for query results
export function dataQueryResultTable(queryKey, queryValueString, queryResultCount) {
    return {
        type: 'DATA_QUERY_RESULT_TABLE',
        payload: {
            queryKey,
            queryValueString,
            queryResultCount
        }

    }
}

//the Querykey serves as a master index for query results and dataSpepPgSuccess
export function updateQueryKey () {
    return {
        type: 'UPDATE_QUERY_KEY',
    }
}

export function updateListKey (listKey) {
    return {
        type: 'UPDATE_LIST_KEY',
        listKey
    }
}

//stores the last query date time range value
export function updateQueryDateTimeRangeValue (queryDateTimeRangeValue) {
    return {
        type: 'UPDATE_QUERY_DATE_TIME_RANGE_VALUE',
        queryDateTimeRangeValue
    }
}

//sores the last query by MRN
export function updateQueryMRNValue (queryMRNValue) {
    return {
        type: 'UPDATE_QUERY_MRN_VALUE',
        queryMRNValue
    }
}

export function PgQueryDateTimeRange(dateTimeRangeValue, queryKey) {

    return (dispatch) => {
        dispatch(dataSpepIsLoading(true));

        axios.post(urlSebia, 
            {
            dateTimeRange: dateTimeRangeValue
        })
        .then((resp) => {
            dispatch(process_PgQueryDateTimeRange(dateTimeRangeValue, queryKey, resp));
        })
        .catch((err) => {
            dispatch(dataSpepHasErrored(true));
            dispatch(dataSpepIsLoading(false));
            console.error(err);
        });
    };
}

export function process_PgQueryDateTimeRange(dateTimeRangeValue, queryKey, resp) {
    return (dispatch) => {
        if (resp.data.error === false) {
            dispatch(dataSpepIsLoading(false));
            dispatch(dataSpepHasErrored(false));
            dispatch(dataSpepPgSuccess(queryKey, dateTimeRangeValue, resp.data.results.Sebia));
            dispatch(min_max_update(0,15));
            dispatch(dataQueryResultTable(queryKey, JSON.stringify(dateTimeRangeValue[0].format('MM/DD/YYYY') + ' to ' + dateTimeRangeValue[1].format('MM/DD/YYYY')), resp.data.results.Sebia.length));
            dispatch(updateQueryKey());
        } else {
            console.log(resp);
            dispatch(dataSpepIsLoading(false));
            dispatch(dataSpepHasErrored(true));
        }
    }
}

export function PgQueryMRN(MRNvalue, queryKey) {


    return (dispatch) => {
        dispatch(dataSpepIsLoading(true));

        axios.post(urlSebia_MRN, {
            MRN_spep: MRNvalue
        })
        .then((resp) => {
            dispatch(process_PgQueryMRN(MRNvalue, queryKey, resp));
        })
        .catch((err) => {
            dispatch(dataSpepHasErrored(true));
            dispatch(dataSpepIsLoading(false));
            console.error(err);
        });
    };
}


export function process_PgQueryMRN (MRNvalue, queryKey, resp) {
    return (dispatch) => {
        if (resp.data.error === false) {
            dispatch(dataSpepIsLoading(false));
            dispatch(dataSpepHasErrored(false));
            dispatch(dataSpepPgSuccess(queryKey, MRNvalue, resp.data.results.Sebia));
            dispatch(min_max_update(0,15));
            dispatch(dataQueryResultTable(queryKey, JSON.stringify(MRNvalue), resp.data.results.Sebia.length));
            dispatch(updateQueryKey());
        } else {
            dispatch(dataSpepIsLoading(false));
            dispatch(dataSpepHasErrored(true));
        }
    }
}

export function PgQueryID_All(IDvalue) {

    return (dispatch) => {
        dispatch(dataSpepIsLoading(true));

        axios.post(urlSebia_ID_M, {
            ID_spep: IDvalue
        })
        .then((resp) => {
            dispatch(process_PgQueryID(resp));
        })
        .catch((err) => {
            dispatch(dataSpepHasErrored(true));
            dispatch(dataSpepIsLoading(false));
            console.error(err);
        });
    };
}



export function process_PgQueryID(resp) {

    return (dispatch) => {
        if (resp.data.error === false) {
            dispatch(dataSpepIsLoading(false));
            dispatch(dataSpepHasErrored(false));
            dispatch(sebiaSubtyping_new(resp.data.results.Sebia));
        } else {
            dispatch(dataSpepIsLoading(false));
            dispatch(dataSpepHasErrored(true));
        }
    }
}

//queue for existing comments

export function PgQueryDatatable_view(IDvalue) {

    return (dispatch) => {

        axios.post(urlDatatable_View, {
            id: IDvalue
        })
        .then((resp) => {
            dispatch(process_PgQueryDatatable_view(resp));
        })
        .catch((err) => {
            console.error(err);
        });
    };
}



export function process_PgQueryDatatable_view(resp) {

    return (dispatch) => {
        if (resp.data.error === false) {
            if (Object.keys(resp.data.results.Sebia).length > 0) {
                dispatch(curve_comment(resp.data.results.Sebia[0].comment));
            } else {
                dispatch(curve_comment(''))
            };
            
        } else {
            dispatch(curve_comment(''))
        }
    }
}

//insert comments

export function PgQueryDatatable_insert(IDvalue, commentvalue) {

    return (dispatch) => {
        console.log(IDvalue, commentvalue);

        axios.post(urlDatatable_Insert, {
            id: IDvalue,
            comment: commentvalue
        })
        .then((resp) => {
            console.log(resp);
        })
        .catch((err) => {
            console.error(err);
        });
    };
}


export function PgQueryMRN_history_only(MRNvalue) {

    return (dispatch) => {
        dispatch(dataSpepIsLoading(true));

        axios.post(urlSebia_MRN, {
            MRN_spep: MRNvalue
        })
        .then((resp) => {
            dispatch(dataSpepIsLoading(false));
            dispatch(dataSpepHistory(resp.data.results.Sebia));
        })
        .catch((err) => {
            dispatch(dataSpepHasErrored(true));
            console.error(err);
        });
    };
}

//handles hidden spep history query result
export function dataSpepHistory (spepHistory) {
    return {
        type: 'DATA_SPEP_HISTORY',
        spepHistory
    }
}

//handles drawer components
export function dataDrawerL1(visible, queryKey, drawerContentL1) {
    return {
        type: 'DATA_DRAWER_L1',
        payload: {
            visible,
            queryKey,
            drawerContentL1,
        }
    }
}

export function dataDrawerL1_SetVisibility(visible) {
    return {
        type: 'DATADRAWERL1_SET_VISIBILITY',
        payload: {
            visible
        }
    }
}

export function dataDrawerL2_DataMassage(drawerContentL2, key) {

            return (dispatch) => {
                dispatch(dataDrawerL2(true, drawerContentL2));
                dispatch(updateListKey(key));
                dispatch(PgQueryID_All(drawerContentL2.spepdata.id));
                dispatch(PgQueryDatatable_view(drawerContentL2.spepdata.id));
                dispatch(dataAxiosFetch(drawerContentL2.spepdata.free1, moment(drawerContentL2.spepdata.data_analisi).subtract(3, 'day').format('MM/DD/YYYY'), moment(drawerContentL2.spepdata.data_analisi).add(1, 'day').format('MM/DD/YYYY')));
                dispatch(PgQueryMRN_history_only(drawerContentL2.spepdata.free1));
            }

}


export function sebiaSubtyping_new(sebiaData) {
    return {
        type: 'SEBIA_SUBTYPING_NEW',
        payload: {
            sebiaData
        }
    }
}

export function sebiaSubtyping_add(sebiaData) {
    return {
        type: 'SEBIA_SUBTYPING_ADD',
        payload: {
            sebiaData
        }
    }
}

export function sebiaSubtyping_Clear() {
    return {
        type: 'SEBIA_SUBTYPING_CLEAR',
    }
}

export function dataDrawerL2(visible, dataDrawerContentL2) {
    return {
        type: 'DATA_DRAWER_L2',
        payload: {
            visible,
            dataDrawerContentL2,
        }
    }
}

export function dataDrawerL2_visibility(visibility) {
    return {
        type: 'DATADRAWERL2_VISIBILITY',
        payload: {
            visibility
        }
    }
}

export function processModalLabs(orderIDLocal, MRNLocal2) {

    return (dispatch) => {
        dispatch(dataIsLoading(true));
        dispatch(modalLabs(true));

        axios.post(urlLabs, {
            MRN: MRNLocal2,
            orderID: orderIDLocal,
        })
        .then((resp) => {
            dispatch(dataIsLoading(false));
            dispatch(dataAxiosLabsSuccess(resp));
            })
        .catch((err) => {
            dispatch(dataHasErrored(true));
            console.error(err);
        });
    };
}

export function processModalRads(orderIDLocal, MRNLocal2) {

    return (dispatch) => {
        dispatch(dataIsLoading(true));
        dispatch(modalRads(true));

        axios.post(urlLabs, {
            MRN: MRNLocal2,
            orderID: orderIDLocal,
        })
        .then((resp) => {
            dispatch(dataIsLoading(false));
            dispatch(dataAxiosLabsSuccess(resp));
            })
        .catch((err) => {
            dispatch(dataHasErrored(true));
            console.error(err);
        });
    };
}

export function processModalNotes(noteIDLocal, MRNLocal2) {
    console.log(noteIDLocal, MRNLocal2);

    return (dispatch) => {
        dispatch(dataIsLoading(true));
        dispatch(modalNotes(true));

        axios.post(urlNotes, {
            MRN: MRNLocal2,
            noteID: noteIDLocal,
        })
        .then((resp) => {
            dispatch(dataIsLoading(false));
            dispatch(dataNotesSuccess(resp));
            })
        .catch((err) => {
            dispatch(dataHasErrored(true));
            console.error(err);
        });
    };
}

//flating data fields

export function curve_comment(curveComment) {
    return {
        type: 'CURVE_COMMENT',
        curveComment
    }
}

export function curve_width(curveWidth) {
    return {
        type: 'CURVE_WIDTH',
        curveWidth
    }
}

export function curve_height(curveHeight) {
    return {
        type: 'CURVE_HEIGHT',
        curveHeight
    }
}

export function curve_c_tension(curveCTension) {
    return {
        type: 'CURVE_C_TENSION',
        curveCTension
    }
}

export function curve_margin(curveMargin) {
    return {
        type: 'CURVE_MARGIN',
        curveMargin
    }
}

export function dataDrawerL3(visible, drawerContent) {
    return {
        type: 'DATA_DRAWER_L3',
        payload: {
            visible,
            drawerContent,
        }
    }
}

export function dataDrawerL4_Set_Visibility(visibility) {
    return {
        type: 'DATA_DRAWER_L4_SET_VISIBILITY',
        payload: {
            visibility
        }
    }
}


export function dataDrawerType(typeValue) {
    return {
        type: 'DATA_DRAWER_TYPE',
        drawerType: typeValue
    }
}

export function dataDrawerCargoSpep(cargoSpepValue) {
    return {
        type: 'DATA_DRAWER_CARGO_SPEP',
        drawerCargoSpep: cargoSpepValue
    }
}

export function modalLabs(modalLabsVisible) {
    return {
        type: 'MODAL_LABS',
        modalLabsVisible
    }
}

export function modalRads(modalRadsVisible) {
    return {
        type: 'MODAL_RADS',
        modalRadsVisible
    }
}

export function modalNotes(modalNotesVisible) {
    return {
        type: 'MODAL_NOTES',
        modalNotesVisible
    }
}



//handles axios request for medication
export function dataHasErrored(bool) {
	return {
		type: 'DATA_HAS_ERRORED',
		hasErrored: bool
	}
}

//handles axios request for medication
export function dataIsLoading(bool) {
	return {

		type: 'DATA_IS_LOADING',
		isLoading: bool
	}
}

//this stores the json med values
export function dataAxiosSuccess(dataValue) {
	return {
		type: 'DATA_AXIOS_SUCCESS',
		dataValue
	}
}

export function dataAxiosLabsSuccess(dataLabsValue) {
    return {
        type: 'DATA_AXIOS_LABS_SUCCESS',
        dataLabsValue
    }
}


//stores the json info values
export function dataInfoAxiosSuccess(dataValueInfo) {
    return {
        type: 'DATA_AXIOS_INFO_SUCCESS',
        dataValueInfo
    }
}


export function dataMedAxiosSuccess(dataMed) {
    return {
        type: 'DATA_MED_AXIOS_SUCCESS',
        dataMed
    }
}

export function dataSpepLabResultsAxiosSuccess(dataSpepLabResults) {
    return {
        type: 'DATA_SPEPLABRESULTS_AXIOS_SUCCESS',
        dataSpepLabResults
    }
}

export function dataSpepInterpAxiosSuccess(dataSpepInterp) {
    return {
        type: 'DATA_SPEP_INTERP_AXIOS_SUCCESS',
        dataSpepInterp
    }
}

export function dataSebiaPGSuccess(dataSebia) {
    return {
        type: 'DATA_SEBIA_PG_SUCCESS',
        dataSebia
    }
}


export function dataOrderLabsSuccess(dataOrderLabs) {
    return {
        type: 'DATA_ORDERS_LABS_SUCCESS',
        dataOrderLabs
    }
}

export function dataNotesSuccess(dataNotes) {
    return {
        type: 'DATA_NOTES_SUCCESS',
        dataNotes
    }
}

//the master Epic query that results in order/note ID numbers
export function dataAxiosFetch(passSearchValue, passDateTimeStart, passDateTimeEnd) {
    return (dispatch) => {
        dispatch(dataIsLoading(true));

        axios.post(urlEpic, {
            MRN: passSearchValue,
            dateTimeStart: passDateTimeStart,
            dateTimeEnd: passDateTimeEnd
        })
        .then((resp) => {
                dispatch(process_dataAxiosFetch(resp));
            })
        .catch((error) => {
            dispatch(dataHasErrored(true));
            dispatch(dataIsLoading(false));
            console.log(error);
        });
    };
}

export function process_dataAxiosFetch(resp) {
    return (dispatch) => {
        if (resp.data.error === false) {
            dispatch(dataIsLoading(false));
            dispatch(dataHasErrored(false));
            dispatch(dataAxiosSuccess(resp));
        } else {
            dispatch(dataIsLoading(false));
            dispatch(dataHasErrored(true));
            dispatch(dataAxiosSuccess({}));

        }
    }
}

//initiate axios call to Sebia
export function dataAxiosFetchSebia(resp) {
    return (dispatch) => {
        axios.post(urlSebia, {
            LastName: resp.data.results.Information.PatientInformation.Name.LastName,
            FirstName: resp.data.results.Information.PatientInformation.Name.FirstName,
        })
        .then((resp_sebia) => {
            dispatch(dataSebiaPGSuccess(resp_sebia.data.results));
            })
        .catch((err) => {
            console.error(err);
        });
    };
}


export function selectLeftNavBar(string) {
    return {
        type: 'SELECT_LEFT_NAV_BAR',
        leftNavBarSelection: string
    }
}

export function selectRightNavBar(string) {
    return {
        type: 'SELECT_RIGHT_NAV_BAR',
        rightNavBarSelection: string
    }
}

export function selectQueryPage(string) {
    return {
        type: 'SELECT_QUERY_PAGE',
        queryPage: string
    }
}

export function processSearchValue(string) {
    return {
        type: 'PROCESS_SEARCH_VALUE',
        searchValue: string
    }
}