import React, { Component, PropTypes } from 'react';
import { Drawer } from 'antd';
import { connect } from 'react-redux';
import History from './history.js';
import Immunotype from './immunotype.js';
import { Input, Table } from 'antd';
import { dataDrawerL4_Set_Visibility } from './actions/data.js';
const { Column, ColumnGroup } = Table;

  
class DrawerContentL4 extends Component {    
    
    handleDrawerClose = () => {
        this.props.drawerClose()
    }

	render () {

        return (
            <div>
                <Drawer
                title="Immunotyping"
                width="90%"
                closable={true}
                onClose={()=>this.handleDrawerClose()}
                visible={this.props.visibleL4.visibility}
                zIndex={4}
                >
                <Immunotype />
                </Drawer>
            </div>
 
            );
        }
}

const mapStateToProps = (state) => {
    return {
        visibleL4: state.dataDrawerL4,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        drawerClose: () => {
            dispatch(dataDrawerL4_Set_Visibility(false));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContentL4);

// export default DrawerContentL1;