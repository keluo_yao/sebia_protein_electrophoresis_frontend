import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './../css/umich.css';
import { Layout, Menu, Icon } from 'antd';
import { LocaleProvider } from 'antd';
import { Button } from 'antd';
import LeftNavBar from './leftnavbar.js';
import AppContent from './appcontent.js';
import HeaderContent from './headercontent.js';
import User_Authentication from './user_authentication.js'
import enUS from 'antd/lib/locale-provider/en_US';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import DrawerManagement from './drawermanagement.js';

const { Header, Footer, Sider, Content } = Layout;

const store = configureStore();

class App extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (
                <LocaleProvider locale={enUS}>
                    <div>
                        <Layout>
                              <Sider style={{background: '#00274C', height:"100vh"}}>
                                <LeftNavBar />
                              </Sider>


           
                                <Layout>

                                    <Content style={{ background: '#FFFFFF' }}>
                                        <div >
                                          <AppContent />
                                          <DrawerManagement />
                                          <User_Authentication />
                                        </div>
                                    </Content>
                                    <Footer style={{ background: '#FFFFFF' }}></Footer>
                                </Layout>

                         </Layout>
                     </div>
                </LocaleProvider>
        );
    }
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
    document.getElementById("root")
);