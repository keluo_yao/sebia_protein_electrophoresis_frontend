import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';
import { connect } from 'react-redux';

const columns = [{
  title: 'FirstName',
  dataIndex: 'FirstName',
  key: 'FirstName',
}, {
  title: 'MiddleName',
  dataIndex: 'MiddleName',
  key: 'MiddleName',
}, {
  title: 'LastName',
  dataIndex: 'LastName',
  key: 'LastName',
}, {
  title: 'DOB',
  dataIndex: 'DOB',
  key: 'DOB',
}, {
  title: 'Gender',
  dataIndex: 'Gender',
  key: 'Gender',
}, {
  title: 'MRN',
  dataIndex: 'MRN',
  key: 'MRN',
},
];

const defaultData = [{
  key: "0",
  FirstName: "N/A",
  MiddleName: "N/A",
  LastName: "N/A",
  DOB: "N/A",
  Gender: "N/A",
  MRN: "N/A",
}];

const demoData = [{
  key: "0",
  FirstName: "Jane",
  MiddleName: "N",
  LastName: "Doe",
  DOB: "1/1/1945",
  Gender: "Female",
  MRN: "123456789",
}];
class PatientInfo extends Component {

	render () {
    if (this.props.checklocal.check === false) {
          if (Object.keys(this.props.dataValueInfo).length > 0){
            return (
              <div>
                  <Table pagination={false} dataSource={this.props.dataValueInfo.data.results.Information2} columns={columns} />
              </div>
              )
        } else {
          return (
            <div>
              <Table pagination={false} dataSource={defaultData} columns={columns} />
            </div>
            )
        } 
      } else {
        if (Object.keys(this.props.dataValueInfo).length > 0){
          return (
            <div>
                <Table pagination={false} dataSource={demoData} columns={columns} />
            </div>
            )
      } else {
        return (
          <div>
            <Table pagination={false} dataSource={defaultData} columns={columns} />
          </div>
          )
      } 
      }


    }


	
}

const mapStateToProps = (state) => {
    return {
        dataValueInfo: state.dataAxiosSuccess,
        checklocal: state.demo_mode,
    };
};

export default connect(mapStateToProps)(PatientInfo);